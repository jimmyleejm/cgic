#include <stdio.h>  
#include "cgic.h"  
#include <string.h>  
#include <stdlib.h>  
#include <sys/types.h>    
#include <unistd.h>  
#include<fcntl.h>
#include<linux/reboot.h>  

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/if_arp.h>


#include <netdb.h>
#include <sys/un.h>
#include <sys/stat.h>

#include <net/route.h>
  
#define SOFILENUM   10  
#define SOFILELEN   20  
  
char ip[20]={0};  
char ip_now[20]={0}; 
char Version[20]={0};
char adev_adress[20]={0};  
char ddev_adress[20]={0};  


char TotleSoNum=0;  


//char check_address[50][10];




enum ErrLog  
{  
    ErrSucceed,  
    ErrOpenField,  
    ErrNoFile  
};  

char *ErrUpNews[]=  
{  
    "",  
    "升级文件上载失败",  
    ""  
};


/* 
Find the '=' pos and get the config dat 
*/  

#define ETHER_ADDR_LEN    6
#define UP    1
#define DOWN    0

#define FIFO_NAME "/tmp/fifo"


int get_mac_addr(char *ifname, char *mac)
{
    int fd, rtn;
    struct ifreq ifr;
    
    if( !ifname || !mac ) {
        return -1;
    }
    fd = socket(AF_INET, SOCK_DGRAM, 0 );
    if ( fd < 0 ) {
        perror("socket");
           return -1;
    }
    ifr.ifr_addr.sa_family = AF_INET;    
    strncpy(ifr.ifr_name, (const char *)ifname, IFNAMSIZ - 1 );

    if ( (rtn = ioctl(fd, SIOCGIFHWADDR, &ifr) ) == 0 )
        memcpy(    mac, (unsigned char *)ifr.ifr_hwaddr.sa_data, 6);
    close(fd);
    return rtn;
}

int set_mac_addr(char *ifname, char *mac)
{
    int fd, rtn;
    struct ifreq ifr;

    if( !ifname || !mac ) {
        return -1;
    }
    fd = socket(AF_INET, SOCK_DGRAM, 0 );
    if ( fd < 0 ) {
        perror("socket");
        return -1;
    }
    ifr.ifr_addr.sa_family = ARPHRD_ETHER;
    strncpy(ifr.ifr_name, (const char *)ifname, IFNAMSIZ - 1 );
    memcpy((unsigned char *)ifr.ifr_hwaddr.sa_data, mac, 6);
    
    if ( (rtn = ioctl(fd, SIOCSIFHWADDR, &ifr) ) != 0 ){
        perror("SIOCSIFHWADDR");
    }
    close(fd);
    return rtn;
}

int if_updown(char *ifname, int flag)
{
    int fd, rtn;
    struct ifreq ifr;        

    if (!ifname) {
        return -1;
    }

    fd = socket(AF_INET, SOCK_DGRAM, 0 );
    if ( fd < 0 ) {
        perror("socket");
        return -1;
    }
    
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, (const char *)ifname, IFNAMSIZ - 1 );

    if ( (rtn = ioctl(fd, SIOCGIFFLAGS, &ifr) ) == 0 ) {
        if ( flag == DOWN )
            ifr.ifr_flags &= ~IFF_UP;
        else if ( flag == UP ) 
            ifr.ifr_flags |= IFF_UP;
        
    }

    if ( (rtn = ioctl(fd, SIOCSIFFLAGS, &ifr) ) != 0) {
        perror("SIOCSIFFLAGS");
    }

    close(fd);

    return rtn;
}

/*
 * Convert Ethernet address string representation to binary data
 * @param    a    string in xx:xx:xx:xx:xx:xx notation
 * @param    e    binary data
 * @return    TRUE if conversion was successful and FALSE otherwise
 */
int
ether_atoe(const char *a, unsigned char *e)
{
    char *c = (char *) a;
    int i = 0;

    memset(e, 0, ETHER_ADDR_LEN);
    for (;;) {
        e[i++] = (unsigned char) strtoul(c, &c, 16);
        if (!*c++ || i == ETHER_ADDR_LEN)
            break;
    }
    return (i == ETHER_ADDR_LEN);
}


/*
 * Convert Ethernet address binary data to string representation
 * @param    e    binary data
 * @param    a    string in xx:xx:xx:xx:xx:xx notation
 * @return    a
 */
char *
ether_etoa(const unsigned char *e, char *a)
{
    char *c = a;
    int i;

    for (i = 0; i < ETHER_ADDR_LEN; i++) {
        if (i)
            *c++ = ':';
        c += sprintf(c, "%02X", e[i] & 0xff);
    }
    return a;
}



int SetipAddr(char *ifname, char *Ipaddr, char *mask,char *gateway)  
	{  
		int fd;  
		//int rc;  
		struct ifreq ifr;	
		struct sockaddr_in *sin;  

	  
		fd = socket(AF_INET, SOCK_DGRAM, 0);  
		if(fd < 0)	
		{  
				perror("socket	 error");		
				return -1;		 
		}  
		memset(&ifr,0,sizeof(ifr));   
		strcpy(ifr.ifr_name,ifname);   
		sin = (struct sockaddr_in*)&ifr.ifr_addr;		
		sin->sin_family = AF_INET;		 
		//IP地址  
		if(inet_aton(Ipaddr,&(sin->sin_addr)) < 0)	   
		{		
			perror("inet_aton	error");	   
			return -2;		 
		}	   
	  
		if(ioctl(fd,SIOCSIFADDR,&ifr) < 0)	   
		{		
			perror("ioctl	SIOCSIFADDR   error");		 
			return -3;		 
		}  
			/*	
			//子网掩码	
		if(inet_aton(mask,&(sin->sin_addr)) < 0)	 
		{		
			perror("inet_pton	error");	   
			return -4;		 
		}	   
		if(ioctl(fd, SIOCSIFNETMASK, &ifr) < 0)  
		{  
			perror("ioctl");  
			return -5;	
		}  
	
		//网关	
	
		memset(&rt, 0, sizeof(struct rtentry));  
		memset(sin, 0, sizeof(struct sockaddr_in));  
		sin->sin_family = AF_INET;	
		sin->sin_port = 0;	
		if(inet_aton(gateway, &sin->sin_addr)<0)  
		{  
		   DEBUG ( "inet_aton error\n" );	
		   return -6;  
		}  
		memcpy ( &rt.rt_gateway, sin, sizeof(struct sockaddr_in));	
		((struct sockaddr_in *)&rt.rt_dst)->sin_family=AF_INET;  
		((struct sockaddr_in *)&rt.rt_genmask)->sin_family=AF_INET;  
		rt.rt_flags = RTF_GATEWAY;	
		if (ioctl(fd, SIOCADDRT, &rt)<0)  
		{  
			//zError( "ioctl(SIOCADDRT) error in set_default_route\n");  
			close(fd);	
			return -1;	
		}  */
		close(fd);	
		return 1;  
	}
	
	/*
	int write_update_flag(char * date)  
	{
			FILE *fd_upda; 
			fd_upda=fopen("/mnt/nand1-1/config/update_falg" ,"w");
			fputs(date,fd_upda);
			fclose(fd_upda); 
	}
*/

int Read_update_flag(void)  
{ 
	 FILE *fd_up;  
   char buffer_up[10];    
    
    if((fd_up = fopen("/mnt/nand1-1/config/update_falg","r")) == NULL)   
    {   
        return 1;   
    }   
  
  
    		memset(buffer_up , 0 , sizeof(buffer_up));    
        fgets(buffer_up,10,fd_up);  
        fclose(fd_up); 
        
        if(buffer_up[0]=='0')
        	{
        		
        		return 1;
        	}
        else
        	{
        		return 0;
        	}
        
          
    
   
}
  
  

void ReadTandaConf(void)  
{  
    FILE *fd;  
    char StrLine[10];    
    char ptr[20];  
    int i=0;  
    if((fd = fopen("/mnt/nand1-1/config/num","r")) == NULL)   
    {   
        return;   
    }   
  
  
    		memset(StrLine , 0 , sizeof(StrLine));    
        fgets(StrLine,10,fd);  
    		strcpy(ip_now,StrLine);
        
          
    
    fclose(fd);     
    
     if((fd = fopen("/mnt/nand1-1/config/version","r")) == NULL)   
    {   
        return;   
    }   
  
  
    		memset(StrLine , 0 , sizeof(StrLine));    
        fgets(StrLine,10,fd);  
    		strcpy(Version,StrLine);
        
          
    
    fclose(fd);                    
      
}  
 
  

  
void ShowIndex(void)  
{  
    FILE *fd;  
    char StrLine[1024];  
    int i=0;  
      
    ReadTandaConf();  
    fprintf(cgiOut, "<!-- 2.0: multipart/form-data is required for file uploads. -->");  
    fprintf(cgiOut, "<form method=\"POST\" enctype=\"multipart/form-data\" ");  
    fprintf(cgiOut, "   action=\"");  
    cgiValueEscape(cgiScriptName);  
    fprintf(cgiOut, "\">\n");  
    fprintf(cgiOut, "<p>\n");  
    fprintf(cgiOut ,"<table  id=\"table\" cellpadding=\"5\" width=\"60%%\" border=\"1px\" align=\"center\"  border-radius=\"5px\">");  
    fprintf(cgiOut ,"<tr bgcolor=\"#E0F0F\"></tr><br>");  
    fprintf(cgiOut,"<tr><td align=\"center\" width=\"20%%\">版本号:</td><td>%s</td></tr>",Version);
    fprintf(cgiOut ,"<tr><td align=\"center\" width=\"20%%\">主机号:</td><td>%s</td></tr>",ip_now);  
    fprintf(cgiOut ,"<tr><td align=\"center\" width=\"20%%\">修改主机号:</td><td><input name=\"ip\" value=\"\"  size=\"30\"></td></tr>");  
    fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"ipset\" value=\"修改\" onclick=\"javaScript:alert('请求已发送')\">");  
    
    if(ip_now[0]=='1')
    	{
    fprintf(cgiOut ,"<tr><td align=\"center\" width=\"20%%\">添加电子围栏:</td><td><input name=\"add_dev\" value=\"\"  size=\"30\"></td></tr>");  
    fprintf(cgiOut ,"<tr><td align=\"center\" width=\"20%%\">删除电子围栏:</td><td><input name=\"del_dev\" value=\"\"  size=\"30\"></td></tr>");  
    fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"Setting\" value=\"确认\" onclick=\"javaScript:alert('请求已发送')\"><input class =\"button\" input type=\"submit\"name=\"check\" value=\"查询\" onclick=\"javaScript:alert('请求已发送')\">");  
    
    
    fprintf(cgiOut ,"<tr><td align=\"center\" width=\"20%%\">添加转换器:</td><td><input name=\"add_zhq\" value=\"\"  size=\"30\"></td></tr>");  
    fprintf(cgiOut ,"<tr><td align=\"center\" width=\"20%%\">删除转换器:</td><td><input name=\"del_zhq\" value=\"\"  size=\"30\"></td></tr>");  
    fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"Zhq_set\" value=\"确认\" onclick=\"javaScript:alert('请求已发送')\"><input class =\"button\" input type=\"submit\"name=\"zhq_check\" value=\"查询\" onclick=\"javaScript:alert('请求已发送')\">");  
    
    }
    
    fprintf(cgiOut ,"<tr><td align=\"center\" width=\"20%%\">升级文件上载:</td>");  
    fprintf(cgiOut , "<td align=\"left\" width=\"20%%\"><div class=\"file-box\"> <input type='text' name='textfield1' id='textfield1' class='txt' /> ");  
    fprintf(cgiOut , "<input type='button' class='btn' value='浏览' /> <input type=\"file\" name=\"updatefile\" class=\"file\" id=\"fileField1\" size=\"28\" onchange=\"document.getElementById('textfield1').value=this.value\" /> </div> ");  
    fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"Update\" value=\"确认\" onclick=\"javaScript:alert('按确认后开始升级，请在页面跳转前不要有任何操作')\">");  
   //fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"Update\" value=\"确认\" onclick=\"window.location = 't3.html'\">");  
    
    fprintf(cgiOut, "</table></form>\n");         
   
}  
 
int debug(char* date) 
{
FILE *fd_debug; 

    if((fd_debug = fopen("/mnt/nand1-1/debug","w")) == NULL)   
    {   
        return;   
    }  
    fputs(date,fd_debug);
    fclose(fd_debug); 
    
}

int set_device(int manner,char *address,char *file_name)
{

	int i=0;
	int j=0;
	int device_at=-1;
	int ret=1;
	char device_add[256][10];
	int  device_point=0;
	FILE *pFile=NULL;
	FILE *oFile=NULL;
	
	
	  pFile=fopen(file_name,"r");
	
    if(pFile!=NULL)
    {
    	while(!feof(pFile))
			{	
				 memset(device_add[device_point]  , 0 , sizeof(device_add[device_point]) );
    		 fgets( device_add[device_point] ,  sizeof(device_add[device_point])  , pFile );
    		 device_point++;
    		 if(device_point>=256)
    		 	{
    		 		return -1;
    		 	}
   		}
   		 fclose(pFile);
   		 
   		 for(i=0;i<device_point;i++)
   		 {
   		 	
   		 	for(j=0;j<3;j++)
   		 	{
   		 		if(device_add[i][j]!=address[j])
   		 			{
   		 				break;
   		 			}
   		 	}
   		 	
   		 	if(j==3)
   		 		{
   		 			//printf("device is exist!\n");
   		 			if(manner==1)
   		 				{
   		 					return 0;
   		 				}
   		 			else
   		 				{
   		 				device_at=i;
   		 			//	fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbspis:</td><td>%d</td></tr>",device_at);
   		 				}
   		 		}
   		 }
   		 
    }
    
    
    if(manner==1)
   {
    oFile=fopen(file_name,"a");
   // fseek(oFile,0,SEEK_END);
    fputs(address,oFile);
     fputs("\n",oFile);
    fclose(oFile);
  	}
  	else
  	{
  		if(device_at!=-1)
  			{
				oFile=fopen(file_name,"w");
				// fseek(oFile,0,SEEK_END);
				for(i=0;i<device_point;i++)
				{
				if(device_at!=i)
					{
				fputs(device_add[i],oFile);
			//	fputs("\n",oFile);
					}
				}
    		fclose(oFile);
    		}
  	}
    
	return 0;
}
int show_device(char * file_name)
{
	int i=0;
	int j=0;
	int device_at=-1;
	int ret=1;
	char device_add[50][10];
	int  device_point=0;
	FILE *pFile=NULL;
	
	 pFile=fopen(file_name,"r");
	
    if(pFile!=NULL)
    {
    	while(!feof(pFile))
			{	
				 memset(device_add[device_point]  , 0 , sizeof(device_add[device_point]) );
    		 fgets( device_add[device_point] ,  sizeof(device_add[device_point])  , pFile );
    		 fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
    		 设备:</td><td>%s</td></tr>",device_add[device_point]);
    		 fprintf(cgiOut, "<BR>\n");
    		 device_point++;
    		 if(device_point>=50)
    		 	{
    		 		return -1;
    		 	}
   		}
   		 fclose(pFile);
   	}
   	return 0;
}

int goto_web(int manner)
{
				if(manner==0)
					{
	 							fprintf(cgiOut, "<HTML><HEAD>\n");  
			    		 //fprintf(cgiOut, "<meta http-equiv=\"Refresh\" content=\"0;URL=http://192.168.7.211/t1.html\">");  
			    		 fprintf(cgiOut, "<meta http-equiv=\"Refresh\" content=\"0;URL=/t1.html\">");  
			    		 fprintf(cgiOut, "</HEAD>");  
			    		 fprintf(cgiOut, "<BODY>");   		 
			    		 fprintf(cgiOut, "</BODY></HTML>\n"); 
    			} 	
    		else
    			{
    				
    					fprintf(cgiOut, "<HTML><HEAD>\n");  
			    		 //fprintf(cgiOut, "<meta http-equiv=\"Refresh\" content=\"0;URL=http://192.168.7.211/t1.html\">");  
			    		 fprintf(cgiOut, "<meta http-equiv=\"Refresh\" content=\"0;URL=/t2.html\">");  
			    		 fprintf(cgiOut, "</HEAD>");  
			    		 fprintf(cgiOut, "<BODY>");   		 
			    		 fprintf(cgiOut, "</BODY></HTML>\n"); 
    				
    			}
    			
    			
    				
}


enum ErrLog UpLoadUpdateFile(void)  
{  
    cgiFilePtr file;  
    FILE *fd;  
    char name[1024];  
  //  char buffer[1024]; 
    char path[50];  
    char contentType[1024];  
    int size; 
    int ret=1; 
    unsigned int got;  
    char *tmp = NULL;  
    
 
    			
    			
    if (cgiFormFileName("updatefile", name, sizeof(name)) != cgiFormSuccess) {  
        return ErrNoFile;  
    }   
    cgiFormFileSize("updatefile", &size);  
    cgiFormFileContentType("updatefile", contentType, sizeof(contentType));  
    if (cgiFormFileOpen("updatefile", &file) != cgiFormSuccess) {  
        return ErrNoFile;  
    }  
    /*write file */  
   // ret=strcmp(name,"zjzj_update.bin");
    
    if(strcmp(name,"zjzj_update.bin")==0 || strcmp(name,"zjzj_core.bin")==0 )  
    	{
    			//goto_web(0);
    			
    			
    			
			    tmp=(char *)malloc(sizeof(char)*size);  
			    strcpy(path , "/mnt/nand1-1/");  
			    strcat(path, name);    
			    fd=fopen(path ,"w+");  
			    if(fd==NULL)  
			    {  
			        return ErrOpenField;  
			    }  
			    while (cgiFormFileRead(file, tmp, size, &got) ==  
			        cgiFormSuccess)  
			    {  
			        fwrite(tmp, size, sizeof(char), fd);  
			    }  
			    cgiFormFileClose(file);  
			    free(tmp);  
			    fclose(fd);  
			    return ErrSucceed;  

    	}
    	else
    		{
    			//goto_web(1);
    		return ErrOpenField;  
    	  }
}  



int send_massge(char manner)
{
	int pipe_fd;
	char buffer='1';
	  
	  
  pipe_fd=open(FIFO_NAME,O_WRONLY);
   if(pipe_fd!=-1)
			    {
			    	  buffer=manner;
			        write(pipe_fd,&buffer,sizeof(buffer));    
			        close(pipe_fd);              
			    }
			    
			    
}


int inet_setroute(char* target,char* netmask)
{
    struct rtentry route;  /* route item struct */
    //char target[128] = {0};
    char gateway[128] = {0};
    //char netmask[128] = {0};
    int action=1;
    struct sockaddr_in *addr;

    int skfd;

    /* clear route struct by 0 */
    memset((char *)&route, 0x00, sizeof(route));

    /* default target is net (host)*/
    route.rt_flags = RTF_UP ;


    {
       
      //  if(!strcmp(*args, "-net"))
        {/* default is a network target */


            //strcpy(target,"224.0.0.0");
            addr = (struct sockaddr_in*) &route.rt_dst;
            addr->sin_family = AF_INET;
            addr->sin_addr.s_addr = inet_addr(target);
       
            //continue;
        }
        /*
        else if(!strcmp(*args, "-host"))
        {
            args++;
            strcpy(target, *args);
            addr = (struct sockaddr_in*) &route.rt_dst;
            addr->sin_family = AF_INET;
            addr->sin_addr.s_addr = inet_addr(target);
            route.rt_flags |= RTF_HOST;
            args++;
            //continue;
        }
        else
        {
        		printf(" -net \n");
            usage();
            return -1;
        }*/
       // if(!strcmp(*args, "netmask"))
        {/* netmask setting */
        	

            //strcpy(netmask, "224.0.0.0");
            addr = (struct sockaddr_in*) &route.rt_genmask;
            addr->sin_family = AF_INET;
            addr->sin_addr.s_addr = inet_addr(netmask);
    
           // continue;
        }

        
    
       // if(!strcmp(*args, "device") || !strcmp(*args, "dev"))
        {/* device setting */
          //  args++;
            route.rt_dev = "eth0";
            //args++;
           // continue;
        }
     
        /* if you have other options, please put them in this place,
          like the options above. */
    }

	  printf("over\n");
    /* create a socket */
    skfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(skfd < 0)
    {
        perror("socket");
        return -1;
    }

    /* tell the kernel to accept this route */
  
        if(ioctl(skfd, SIOCADDRT, &route) < 0)
        {
            perror("SIOCADDRT");
            close(skfd);
            return -1;
        }
    
    (void) close(skfd);
    return 0;
}

int read_file_num ( char* file,char* date ,int num)
{
	FILE* fp_r ;
	if ( ( fp_r=fopen ( file,"r" ) ) ==NULL )
	{
		printf ( "can not open the file. read_file\n" );
		return -1;
	}
	//fgets ( date,sizeof ( date ),fp_r ); jimmy at there sizeof ( date ) is 4;
	fgets ( date,num,fp_r );
	fclose ( fp_r );
	return 0;
}




int send_rec_date(char * s_date,int mean,int type)
{
	int sockfd;
	struct sockaddr_in addr_ser;
	int err;
	char send_date[1024];
	fd_set rset;
	struct timeval tm;
	int arg =1;
  int i;
  char ip[32];
  char ch_port[8];
  int port;
  memset(ip,0,sizeof(ip));
  read_file_num("/mnt/nand1-1/config/pt_ip",ip ,32);
  read_file_num ( "/mnt/nand1-1/config/pt_port",ch_port ,8);
	port=atoi ( ch_port );
	
	
	sockfd=socket ( AF_INET,SOCK_STREAM,0 );
	if ( sockfd==-1 )
	{
		return -1;
	}

	bzero ( &addr_ser,sizeof ( addr_ser ) );
	addr_ser.sin_family=AF_INET;
	addr_ser.sin_addr.s_addr=inet_addr (ip);
	addr_ser.sin_port=htons ( port );

	err = ioctl(sockfd,FIONBIO,(int)&arg);
	for(i=0;i<2;)
	{
		   err=connect(sockfd,(struct sockaddr *)&addr_ser,sizeof(addr_ser));
		   
		   if(err!=0)
		   {
		   		FD_ZERO(&rset);
				FD_SET(sockfd, &rset);
				tm.tv_sec = 0;
				tm.tv_usec = 500000;

				if ( select(sockfd + 1, NULL, &rset, NULL, &tm) <= 0)
				{
					 printf("connect error err is %d\n",err);
					i++;
					if(i>=2)
						{
						FD_CLR(sockfd, &rset);
						close(sockfd);
						return -1;
						}
				}

				else if (FD_ISSET(sockfd, &rset))
				{
				   FD_CLR(sockfd, &rset);
				   break;
				}
				
				
				/*
			   printf("connect error err is %d\n",err);
			   i++;
			  // if(i>=10)
			   	{
				 return -1;
			   	}
			 //  sleep(1);
			  usleep(500000);*/
		   }

		   else
		   	{
				break;
		   	}
		}
/*
	err=connect ( sockfd, ( struct sockaddr* ) &addr_ser,sizeof ( addr_ser ) );
	if ( err==-1 )
	{
		close ( sockfd );
		return -1;

	}*/
	if(mean==1)
		{
	   sprintf(send_date,"<?xml version=\"1.0\"encoding=\"utf-8\"?><body><uuid>3995006D-B409-46F8-B833-48D810C03FD0</uuid><command>AddLMem</command><data>Ip=192.168.7.211,Port=6666,Type=1,Number=%s</data></body>",s_date);
		}
	else
		{
		 sprintf(send_date,"<?xml version=\"1.0\"encoding=\"utf-8\"?><body><uuid>3995006D-B409-46F8-B833-48D810C03FD0</uuid><command>DelMem</command><data>Ip=192.168.7.211,Port=6666,Type=1,Number=%s</data></body>",s_date);
		}
	
	//send ( sockfd,"rec_message",strlen ( "rec_message" ),0 );
	send ( sockfd,send_date,strlen ( send_date )+1,0 );
	close ( sockfd );
	return 0;
}



  
int cgiMain()   
{  
	
		
	//	char ip[81];
		char tmp_mac[100];
		char tmp_ip[100];
		char mac_add[100];
		int len;
		int len_add,len_del;
		FILE *fp_mac;
		int i;
		 char send_date[1024];
		//char file_name[128]; 
	

		
		
		//init_debug();
    /* Send the content type, letting the browser know this is HTML */  
    cgiHeaderContentType("text/html");  
    //printf("Content-type:text/html;charset=utf-8\n\n");  
    /* Top of the page */  
    fprintf(cgiOut, "<HTML><HEAD>\n");  
     fprintf(cgiOut, "<TITLE>上海跃天</TITLE>\n");  

    fprintf(cgiOut , "<style>#table{border-radius:20px;border:0px;background:#87CEFA;}#table td{color:##00CED1;color:#0F0F0F0F;border:0px;}#h1{color:#CCCCCC;}");  
    fprintf(cgiOut, "input{height:30px;color:#0;border : 0;background:#F0FFFF;}.button{margin-right:10px;background:#4876FF;width:80px;border-radius:5px;color:#FAF0E6;}");  
    fprintf(cgiOut, ".button:active{background:#666666;}.file-box{ position:relative;width:340px;} .txt{ height:30px;; border:1px solid #CCCCCC; width:180px;} ");  
    fprintf(cgiOut, ".btn{ background-color:#4876FF; border-radius:5px solid #CDCDCD;height:30px; width:70px;color:#FAF0E6;} .file{ position:absolute; top:0; right:80px; height:30px; filter:alpha(opacity:0);opacity: 0;width:260px } </style>");  
 
      
    fprintf(cgiOut, "</HEAD>");  
    
    fprintf(cgiOut, "<BODY style=\"background:#FFFFFF\"><h1 id = \"h1\"align=\"center\"> </h1>");  
    
     if ((cgiFormSubmitClicked("ipset") == cgiFormSuccess))  
    {  
    			
    		
				cgiFormStringSpaceNeeded("ip", &len);
						if((len-1) == 1)
						{
		    				cgiFormStringNoNewlines("ip", ip, 20);
		
						 if(ip[0]>='0' && ip[0]<='9' )			
							  {	
								sprintf(tmp_ip,"192.168.7.21%c",ip[0]);		
								SetipAddr("eth0",tmp_ip,"255.255.255.0","192.168.7.0");	
		    				
		    				sprintf(tmp_mac,"00:02:AC:55:82:1%c",ip[0]);		
		    				ether_atoe(tmp_mac,mac_add);
								set_mac_addr("eth0",mac_add);
								
								
								if((fp_mac=fopen("/mnt/nand1-1/config/macadress","w"))==NULL)
													{
													//printf("can not open command passwd the file.\n");
													return -1;
													}
										fprintf(fp_mac,"#!/bin/sh\n");		
										fprintf(fp_mac,"/sbin/ifconfig eth0 192.168.7.21%c netmask 255.255.255.0 broadcast 192.168.7.255\n",ip[0]);							
										fprintf(fp_mac,"/sbin/ifconfig eth0 hw ether 00:02:AC:55:82:1%c\n",ip[0]);
										
										fclose(fp_mac);
								
								if((fp_mac=fopen("/mnt/nand1-1/config/num","w"))==NULL)
													{
													//printf("can not open command passwd the file.\n");
													return -1;
													}
														
										fprintf(fp_mac,"%c\n",ip[0]);
										
										fclose(fp_mac);	
										
									inet_setroute("192.168.0.0","255.255.0.0");
									inet_setroute("224.0.0.0","224.0.0.0");
									
									
								strcpy(ip_now,ip);
		    			  fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
		    			  主机号修改为:</td><td>%s</td></tr>",ip);
		    			  		send_massge('1');
		    			  }
		    			  else
		    			  fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
								请输入一位主机号码!</td></tr>");	
				
				}
    	
				else
				{
				fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
				请输入一位主机号码!</td></tr>");	
				}
				
    } 
    
    // fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">111&nbsp&nbsp&nbsp&nbsp硬件版本号:</td><td>%s</td></tr>","123");
    if ((cgiFormSubmitClicked("Setting") == cgiFormSuccess))  
    {  
    			
    			cgiFormStringSpaceNeeded("add_dev", &len_add);
    			cgiFormStringSpaceNeeded("del_dev", &len_del);
    			
    			
    				if((len_add-1) == 3  )
    				{
    				
    							cgiFormStringNoNewlines("add_dev", adev_adress, 20);
    							if(adev_adress[0]>='0' && adev_adress[0]<='9'  && adev_adress[1]>='0' && adev_adress[1]<='9' && adev_adress[2]>='0' && adev_adress[2]<='9')			
									{
    							set_device(1,adev_adress,"/mnt/nand1-1/config/device_address");
    							show_device("/mnt/nand1-1/config/device_address");
    							send_rec_date(adev_adress,2,1);
    							
    							}
    							else
    							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
    							请输入三位数字!</td></tr>");
    		
    				}
    				else 	if((len_del-1) == 3)
    				{
    					    cgiFormStringNoNewlines("del_dev", ddev_adress, 20);
    					    
    					 		if(ddev_adress[0]>='0' && ddev_adress[0]<='9'  && ddev_adress[1]>='0' && ddev_adress[1]<='9' && ddev_adress[2]>='0' && ddev_adress[2]<='9')			
									{
    							set_device(0,ddev_adress,"/mnt/nand1-1/config/device_address");
    							show_device("/mnt/nand1-1/config/device_address");
    							send_rec_date(ddev_adress,2,1);
    							//sprintf(send_date,"<?xml version=\"1.0\"encoding=\"utf-8\"?><body><uuid>3995006D-B409-46F8-B833-48D810C03FD0</uuid><command>DelMem</command><data>Ip=192.168.7.211,Port=6666,Type=1,Number=%s</data></body>",adev_adress);
    							}
    							else
    							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
    							请输入三位数字!</td></tr>");
    				}
    				else
    				{
    					fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
    				请输入三位地址码!</td></tr>");		
    				}
    				       
    }  
    
    
      if ((cgiFormSubmitClicked("Zhq_set") == cgiFormSuccess))  
    {  
    			
    			cgiFormStringSpaceNeeded("add_zhq", &len_add);
    			cgiFormStringSpaceNeeded("del_zhq", &len_del);
    			
    			
    				if((len_add-1) == 3  )
    				{
    							cgiFormStringNoNewlines("add_zhq", adev_adress, 20);
    							if(adev_adress[0]>='0' && adev_adress[0]<='9'  && adev_adress[1]>='0' && adev_adress[1]<='9' && adev_adress[2]>='0' && adev_adress[2]<='9')			
									{
    							set_device(1,adev_adress,"/mnt/nand1-1/config/zhq_address");
    							show_device("/mnt/nand1-1/config/zhq_address");
    							}
    							else
    							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
    							请输入三位数字!</td></tr>");
    			
    				}
    				else 	if((len_del-1) == 3)
    				{
    							cgiFormStringNoNewlines("del_zhq", ddev_adress, 20);
    								if(ddev_adress[0]>='0' && ddev_adress[0]<='9'  && ddev_adress[1]>='0' && ddev_adress[1]<='9' && ddev_adress[2]>='0' && ddev_adress[2]<='9')			
									{
    							set_device(0,ddev_adress,"/mnt/nand1-1/config/zhq_address");
    							show_device("/mnt/nand1-1/config/zhq_address");
    							}
    								else
    							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
    							请输入三位数字!</td></tr>");
    				}
    				else
    				{
    					fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
    				请输入三位地址码!</td></tr>");		
    				}
    				
    				
    			 // fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp添加设备:</td><td>%s</td></tr>",adev_adress);
    				//fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp删除设备:</td><td>%s</td></tr>",ddev_adress);
       
    }  
    
    
    
     if ((cgiFormSubmitClicked("check") == cgiFormSuccess))  
    {  
    			
    		show_device("/mnt/nand1-1/config/device_address");
    } 
    
     if ((cgiFormSubmitClicked("zhq_check") == cgiFormSuccess))  
    {  
    			
    		show_device("/mnt/nand1-1/config/zhq_address");
    } 
    
    
    if ((cgiFormSubmitClicked("Update") == cgiFormSuccess))  
    {  
    	
			//if(Read_update_flag())
				//{
					
						//write_update_flag("1");
					   
							
							
    			i=UpLoadUpdateFile();
    			
			
    		 	if(i==0)
    		 	{
    		 		//write_update_flag("0");
    		 	//printf("正在更新文件请稍等");  	
    		 	goto_web(0);
    		 	send_massge('1');
    		 	}
    		 	else
    			{
    				//write_update_flag("0");
    			goto_web(1);
    			} 
    		//}
    		
    	
    		
    } 
    
    
    ShowIndex();  
    fprintf(cgiOut, "</BODY></HTML>\n");  
   // fprintf(cgiOut, "</HTML>\n"); 
    return 0;  
}  


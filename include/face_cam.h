#ifndef __FACE_CAM_H__

#define __FACE_CAM_H__

#define ALG_FRAME_START 	0x55BB10A1
#define ALG_CMDFRAME_START  0x55BB10A0


typedef struct{

	unsigned int startmark;	
	unsigned char ver;
	unsigned char flag;
	unsigned char cmd1;  // 
	unsigned char cmd2;	
	unsigned short size;
} alg_cmdframe_t;


typedef struct{

	unsigned int startmark;
	unsigned int framesize;
	unsigned char ver;
	unsigned char flag;
	unsigned char cmd1;  // 
	unsigned char cmd2;	
	
} alg_frame_t;


int SendConnect(char *);
int ComThread();
void Setparamemt(int);
void HeartBeat();


#endif
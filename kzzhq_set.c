#include <stdio.h>  
#include "cgic.h"  
#include <string.h>  
#include <stdlib.h>  
#include <sys/types.h>    
#include <unistd.h>    
#include <fcntl.h>
#include<linux/reboot.h>  

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/if_arp.h>

#include <netdb.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <net/route.h>


  
#define SOFILENUM   10  
#define SOFILELEN   20  
  
char ip[20]={0}; 
char ip_now[20]={0}; 
char netmask_set[20]={0}; 
char netmask_now[20]={0}; 
char gateway_set[20]={0}; 
char gateway_now[20]={0}; 
char macaddr_set[20]={0}; 
char macaddr_now[20]={0}; 
 
char cam_ip[20]={0}; 
char cam_ip_now[20]={0}; 

 
char Version[20]={0};
char adev_adress[20]={0};  
char ddev_adress[20]={0};  
char ter_num[8];
char ter_num_now[8];

char TotleSoNum=0;  


//char check_address[50][10];

char *def_type[] = {
	"Red",
	"Green",
	"Blue"
};


FILE *fd_debug; 

enum ErrLog  
{  
    ErrSucceed,  
    ErrOpenField,  
    ErrNoFile  
};  

/* 
Find the '=' pos and get the config dat 
*/  

#define ETHER_ADDR_LEN    6
#define UP    1
#define DOWN    0

#define PORT 6666

#define FIFO_NAME "/tmp/fifo"
#define FOFI_NAME "/tmp/fofi"


int write_file ( char* file,char* date )
{
//int fd = open (journal_filename, O_WRONLY | O_CREAT | O_APPEND, 0660);
FILE* fp_w;

int fd_w;
if ( ( fp_w=fopen ( file,"w" ) ) ==NULL )
{
	//printf ( "can not open the file. write_file\n" );
	return -1;
}
fd_w = fileno(fp_w);
fputs ( date,fp_w );
fputs ( "\n",fp_w );

fflush(fp_w);
fsync (fd_w);
fclose ( fp_w );



}


int read_file ( char* file,char* date )
{
	FILE* fp_r ;
	if ( ( fp_r=fopen ( file,"r" ) ) ==NULL )
	{
		//printf ( "can not open the file. read_file\n" );
		return -1;
	}
	fgets ( date,20,fp_r );//jimmy ??? fgets ( date,sizeof(date),fp_r );
	fclose ( fp_r );
	return 0;
}


int get_mac_addr(char *ifname, char *mac)
{
    int fd, rtn;
    struct ifreq ifr;
    
    if( !ifname || !mac ) {
        return -1;
    }
    fd = socket(AF_INET, SOCK_DGRAM, 0 );
    if ( fd < 0 ) {
        perror("socket");
           return -1;
    }
    ifr.ifr_addr.sa_family = AF_INET;    
    strncpy(ifr.ifr_name, (const char *)ifname, IFNAMSIZ - 1 );

    if ( (rtn = ioctl(fd, SIOCGIFHWADDR, &ifr) ) == 0 )
        memcpy(    mac, (unsigned char *)ifr.ifr_hwaddr.sa_data, 6);
    close(fd);
    return rtn;
}

int set_mac_addr(char *ifname, char *mac)
{
    int fd, rtn;
    struct ifreq ifr;

    if( !ifname || !mac ) {
        return -1;
    }
    fd = socket(AF_INET, SOCK_DGRAM, 0 );
    if ( fd < 0 ) {
        perror("socket");
        return -1;
    }
    ifr.ifr_addr.sa_family = ARPHRD_ETHER;
    strncpy(ifr.ifr_name, (const char *)ifname, IFNAMSIZ - 1 );
    memcpy((unsigned char *)ifr.ifr_hwaddr.sa_data, mac, 6);
    
    if ( (rtn = ioctl(fd, SIOCSIFHWADDR, &ifr) ) != 0 ){
        perror("SIOCSIFHWADDR");
    }
    close(fd);
    return rtn;
}

int if_updown(char *ifname, int flag)
{
    int fd, rtn;
    struct ifreq ifr;        

    if (!ifname) {
        return -1;
    }

    fd = socket(AF_INET, SOCK_DGRAM, 0 );
    if ( fd < 0 ) {
        perror("socket");
        return -1;
    }
    
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, (const char *)ifname, IFNAMSIZ - 1 );

    if ( (rtn = ioctl(fd, SIOCGIFFLAGS, &ifr) ) == 0 ) {
        if ( flag == DOWN )
            ifr.ifr_flags &= ~IFF_UP;
        else if ( flag == UP ) 
            ifr.ifr_flags |= IFF_UP;
        
    }

    if ( (rtn = ioctl(fd, SIOCSIFFLAGS, &ifr) ) != 0) {
        perror("SIOCSIFFLAGS");
    }

    close(fd);

    return rtn;
}

/*
 * Convert Ethernet address string representation to binary data
 * @param    a    string in xx:xx:xx:xx:xx:xx notation
 * @param    e    binary data
 * @return    TRUE if conversion was successful and FALSE otherwise
 */
int
ether_atoe(const char *a, unsigned char *e)
{
    char *c = (char *) a;
    int i = 0;

    memset(e, 0, ETHER_ADDR_LEN);
    for (;;) {
        e[i++] = (unsigned char) strtoul(c, &c, 16);
        if (!*c++ || i == ETHER_ADDR_LEN)
            break;
    }
    return (i == ETHER_ADDR_LEN);
}


/*
 * Convert Ethernet address binary data to string representation
 * @param    e    binary data
 * @param    a    string in xx:xx:xx:xx:xx:xx notation
 * @return    a
 */
char *
ether_etoa(const unsigned char *e, char *a)
{
    char *c = a;
    int i;

    for (i = 0; i < ETHER_ADDR_LEN; i++) {
        if (i)
            *c++ = ':';
        c += sprintf(c, "%02X", e[i] & 0xff);
    }
    return a;
}



int SetipAddr(char *ifname, char *Ipaddr, char *mask,char *gateway)  
	{  
		int fd;  
		//int rc;  
		struct ifreq ifr;	
		struct sockaddr_in *sin;  

	  
		fd = socket(AF_INET, SOCK_DGRAM, 0);  
		if(fd < 0)	
		{  
				perror("socket	 error");		
				return -1;		 
		}  
		memset(&ifr,0,sizeof(ifr));   
		strcpy(ifr.ifr_name,ifname);   
		sin = (struct sockaddr_in*)&ifr.ifr_addr;		
		sin->sin_family = AF_INET;		 
		//IP地址  
		if(inet_aton(Ipaddr,&(sin->sin_addr)) < 0)	   
		{		
			perror("inet_aton	error");	   
			return -2;		 
		}	   
	  
		if(ioctl(fd,SIOCSIFADDR,&ifr) < 0)	   
		{		
			perror("ioctl	SIOCSIFADDR   error");		 
			return -3;		 
		}  
		//子网掩码	
		if(inet_aton(mask,&(sin->sin_addr)) < 0)	 
		{		
			perror("inet_pton	error");	   
			return -4;		 
		}	   
		if(ioctl(fd, SIOCSIFNETMASK, &ifr) < 0)  
		{  
			perror("ioctl");  
			return -5;	
		}  
		//网关	
		/*
		memset(&rt, 0, sizeof(struct rtentry));  
		memset(sin, 0, sizeof(struct sockaddr_in));  
		sin->sin_family = AF_INET;	
		sin->sin_port = 0;	
		if(inet_aton(gateway, &sin->sin_addr)<0)  
		{  
		   DEBUG ( "inet_aton error\n" );	
		   return -6;  
		}  
		memcpy ( &rt.rt_gateway, sin, sizeof(struct sockaddr_in));	
		((struct sockaddr_in *)&rt.rt_dst)->sin_family=AF_INET;  
		((struct sockaddr_in *)&rt.rt_genmask)->sin_family=AF_INET;  
		rt.rt_flags = RTF_GATEWAY;	
		if (ioctl(fd, SIOCADDRT, &rt)<0)  
		{  
			//zError( "ioctl(SIOCADDRT) error in set_default_route\n");  
			close(fd);	
			return -1;	
		}  */
		close(fd);	
		return 1;  
	}
	
	
	


  
  

void ReadTandaConf(void)  
{  
    FILE *fd;  
    char StrLine[64];    
    char ptr[20];  
    int i=0;  
    
   	read_file ( "/mnt/nand1-1/config/ipadress",ip_now);
		read_file ( "/mnt/nand1-1/config/netmask",netmask_now);
		read_file ( "/mnt/nand1-1/config/gateway",gateway_now);
		read_file ( "/mnt/nand1-1/config/macaddr",macaddr_now);
		read_file ( "/mnt/nand1-1/config/cam_ipadd",cam_ip_now);
                        
      
}  



  
void ShowIndex(void)  
{  
    FILE *fd;  
    char StrLine[1024];  
    int i=0;  
      
    ReadTandaConf();  
    
    //fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp//&nbsp&nbsp&nbsp&nbsp\门牌号修改为:</td><td>%s</td></tr>","123");
			   	
			  
	  fprintf(cgiOut, "<!-- 2.0: multipart/form-data is required for file uploads. -->");  
    fprintf(cgiOut, "<form method=\"POST\" enctype=\"multipart/form-data\" ");  
    fprintf(cgiOut, "   action=\"");  
    cgiValueEscape(cgiScriptName);  
    fprintf(cgiOut, "\">\n");  
    fprintf(cgiOut, "<p>\n"); 
    
    		  
			fprintf(cgiOut ,"<p>");   
			fprintf(cgiOut ,"<br/>");   
			fprintf(cgiOut ,"<br/>");      
			fprintf(cgiOut ,"</p>");   	  
			
			fprintf(cgiOut ,"<hr width=\"100%%\" size=\"10\" color=\"#0000ff\" align=\"center\">");
			 
			fprintf(cgiOut ,"<p>");   
			fprintf(cgiOut ,"<br/>");   
			fprintf(cgiOut ,"<br/>");      
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"本机ip地址：<input value=\"%s\" type=\"text\" name=\"local_ip\" id=\"local_ip\" />",ip_now); 
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"本机子网掩码：<input value=\"%s\" type=\"text\" name=\"local_netmask\" id=\"local_netmask\" />",netmask_now); 
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"本机默认网关：<input value=\"%s\" type=\"text\" name=\"local_gateway\" id=\"local_gateway\" />",gateway_now); 
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"本机物理地址：<input value=\"%s\" type=\"text\" name=\"local_macadd\" id=\"local_macadd\" />",macaddr_now); 
			fprintf(cgiOut ,"</p>"); 
			
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"控制器ip地址：<input value=\"%s\" type=\"text\" name=\"cam_ipaddr\" id=\"cam_ipaddr\" />",cam_ip_now); 
			fprintf(cgiOut ,"</p>");  
			
			
			fprintf(cgiOut ,"<p align=\"center\">");  
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"local_par_set\" value=\"确定\" onclick=\"javaScript:alert('请求已发送')\">");  
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"clean_set\" value=\"取消\" >");   
			fprintf(cgiOut ,"</p>");  
			

			



		  
			
			
   
}  
 
int init_debug() 
{

    if((fd_debug = fopen("/mnt/nand1-1/debug","w")) == NULL)   
    {   
        return;   
    }  
}

enum ErrLog UpLoadUpdateFile(void)  
{  
    cgiFilePtr file;  
    FILE *fd;  
    char name[1024];  
  //  char buffer[1024]; 
    char path[50];  
    char contentType[1024];  
    int size; 
    int ret=1; 
    unsigned int got;  
    char *tmp = NULL;  
    if (cgiFormFileName("updatefile", name, sizeof(name)) != cgiFormSuccess) {  
        return ErrNoFile;  
    }   
    cgiFormFileSize("updatefile", &size);  
    cgiFormFileContentType("updatefile", contentType, sizeof(contentType));  
    if (cgiFormFileOpen("updatefile", &file) != cgiFormSuccess) {  
        return ErrNoFile;  
    }  
    /*write file */  
    ret=strcmp(name,"zhq_update.bin");
    
    if(ret==0)  
    	{
    			//goto_web(0);
			    tmp=(char *)malloc(sizeof(char)*size);  
			    strcpy(path , "/mnt/nand1-1/");  
			    strcat(path, name);    
			    fd=fopen(path ,"w+");  
			    if(fd==NULL)  
			    {  
			        return ErrOpenField;  
			    }  
			    while (cgiFormFileRead(file, tmp, size, &got) ==  
			        cgiFormSuccess)  
			    {  
			        fwrite(tmp, size, sizeof(char), fd);  
			    }  
			    cgiFormFileClose(file);  
			    free(tmp);  
			    fclose(fd);  
			    return ErrSucceed;  

    	}
    	else
    		{
    		//	goto_web(1);
    		return ErrOpenField;  
    	  }
}  
  
int send_massge(char *date)
{
	int pipe_fd;
	    
  pipe_fd=open(FIFO_NAME,O_WRONLY);
   if(pipe_fd!=-1)
    {
        write(pipe_fd,date,strlen(date));    
        close(pipe_fd);              
    }
			    
	return 0;	    
}

int rec_massge()
{
	int pipe_re;
	char buffer[32];
  pipe_re=open(FOFI_NAME,O_RDONLY);
   if(pipe_re!=-1)
    {

        read(pipe_re,buffer,sizeof(buffer));  
        //printf("the read data is %s\n",buffer);  
        close(pipe_re);  
        
        if(( strstr(buffer, "succeed")) != NULL)
        	{
        		return 0;
        	}
        else
        	{
        		 return -1;  
        	}  
               
    }
	
	
	return -1;		    
			    
}

int inet_setroute(char *target,char *netmask)
{
    struct rtentry route;  /* route item struct */
   // char target[128] = {0};
    char gateway[128] = {0};
  // char netmask[128] = {0};
    int action=1;
    struct sockaddr_in *addr;

    int skfd;

    /* clear route struct by 0 */
    memset((char *)&route, 0x00, sizeof(route));

    /* default target is net (host)*/
    route.rt_flags = RTF_UP ;


    {
       
      //  if(!strcmp(*args, "-net"))
        {/* default is a network target */


            //strcpy(target,"224.0.0.0");
            //strcpy(target,net);
            addr = (struct sockaddr_in*) &route.rt_dst;
            addr->sin_family = AF_INET;
            addr->sin_addr.s_addr = inet_addr(target);
       
            //continue;
        }
        /*
        else if(!strcmp(*args, "-host"))
        {
            args++;
            strcpy(target, *args);
            addr = (struct sockaddr_in*) &route.rt_dst;
            addr->sin_family = AF_INET;
            addr->sin_addr.s_addr = inet_addr(target);
            route.rt_flags |= RTF_HOST;
            args++;
            //continue;
        }
        else
        {
        		printf(" -net \n");
            usage();
            return -1;
        }*/
       // if(!strcmp(*args, "netmask"))
        {/* netmask setting */
        	

           // strcpy(netmask, "224.0.0.0");
           // strcpy(netmask, mask);
            addr = (struct sockaddr_in*) &route.rt_genmask;
            addr->sin_family = AF_INET;
            addr->sin_addr.s_addr = inet_addr(netmask);
    
           // continue;
        }

        
    
       // if(!strcmp(*args, "device") || !strcmp(*args, "dev"))
        {/* device setting */
          //  args++;
            route.rt_dev = "eth0";
            //args++;
           // continue;
        }
     
        /* if you have other options, please put them in this place,
          like the options above. */
    }

	  //printf("over\n");
    /* create a socket */
    skfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(skfd < 0)
    {
        perror("socket");
        return -1;
    }

    /* tell the kernel to accept this route */
  
        if(ioctl(skfd, SIOCADDRT, &route) < 0)
        {
            perror("SIOCADDRT");
            close(skfd);
            return -1;
        }
    
    (void) close(skfd);
    return 0;
}

int goto_web(int manner)
{
				if(manner==0)
					{
	 							fprintf(cgiOut, "<HTML><HEAD>\n");  
			    		 //fprintf(cgiOut, "<meta http-equiv=\"Refresh\" content=\"0;URL=http://192.168.7.211/t1.html\">");  
			    		 fprintf(cgiOut, "<meta http-equiv=\"Refresh\" content=\"0;URL=/t1.html\">");  
			    		 fprintf(cgiOut, "</HEAD>");  
			    		 fprintf(cgiOut, "<BODY>");   		 
			    		 fprintf(cgiOut, "</BODY></HTML>\n"); 
    			} 	
    		else
    			{
    				
    					fprintf(cgiOut, "<HTML><HEAD>\n");  
			    		 //fprintf(cgiOut, "<meta http-equiv=\"Refresh\" content=\"0;URL=http://192.168.7.211/t1.html\">");  
			    		 fprintf(cgiOut, "<meta http-equiv=\"Refresh\" content=\"0;URL=/t2.html\">");  
			    		 fprintf(cgiOut, "</HEAD>");  
			    		 fprintf(cgiOut, "<BODY>");   		 
			    		 fprintf(cgiOut, "</BODY></HTML>\n"); 
    				
    			}
    			
    			
    				
}

int check_input_macadd(char *check_date,int len)
{
int point_num=0;
int i;

		for(i=0;i<len-1;i++)
		{
			
			//fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    //				DATE IS %c </td></tr>",check_date[i]);	
	    				
			if( check_date[i]<'0' || check_date[i]>'9' )
				{
					if( check_date[i]<'a' || check_date[i]>'f' )
						{
								if( check_date[i]<'A' || check_date[i]>'F' )
							{
									if(check_date[i]!=':')
									{
											return -1;
				  				}
				  				else
									{
											point_num++;
									}
							}
					  }
				}
			
		}
		
	 	if(point_num!=5)			
	 	{
				return -1;
	 	}  
	 	else
	 	{
			return 0;
	 	}	
  	 	
  	 	return 0;
}

int check_input_ip(char *check_date,int len)
{
int point_num=0;
int i;

		for(i=0;i<len-1;i++)
		{
			if( check_date[i]<'0' || check_date[i]>'9' )
				{
					if(check_date[i]!='.')
					{
							return -1;
  				}
  				else
					{
							point_num++;
					}
				}
			
		}
		
	 	if(point_num!=3)			
	 	{
				return -1;
	 	}  
	 	else
	 	{
			return 0;
	 	}	
  	 	
  	 	return 0;
}




int cgiMain()   
{  
		char tmp_mac[100];
		char tmp_ip[100];
		char mac_add[100];
		int len;
		int len_add,len_del;
		FILE *fp_mac;
		int i;
	  int ret=0;
	  char tmp_buffer[64];
	  int TypeChoice;
		//init_debug();
    /* Send the content type, letting the browser know this is HTML */  
    cgiHeaderContentType("text/html");  
    //printf("Content-type:text/html;charset=utf-8\n\n");  
    /* Top of the page */  
    fprintf(cgiOut, "<HTML><HEAD>\n");  
    fprintf(cgiOut, "<TITLE>上海跃天</TITLE>\n");  
/*
    fprintf(cgiOut , "<style>#table{border-radius:20px;border:0px;background:#87CEFA;}#table td{color:##00CED1;color:#0F0F0F0F;border:0px;}#h1{color:#CCCCCC;}");  
    fprintf(cgiOut, "input{height:30px;color:#0;border : 0;background:#F0FFFF;}.button{margin-right:10px;background:#4876FF;width:80px;border-radius:5px;color:#FAF0E6;}");  
    fprintf(cgiOut, ".button:active{background:#666666;}.file-box{ position:relative;width:340px;} .txt{ height:30px;; border:1px solid #CCCCCC; width:180px;} ");  
    fprintf(cgiOut, ".btn{ background-color:#4876FF; border-radius:5px solid #CDCDCD;height:30px; width:70px;color:#FAF0E6;} .file{ position:absolute; top:0; right:80px; height:30px; filter:alpha(opacity:0);opacity: 0;width:260px } </style>");  
 */
      
    fprintf(cgiOut, "</HEAD>");  
    
    fprintf(cgiOut, "<BODY bgcolor=\"#FFFFFF\">");  
    
     if ((cgiFormSubmitClicked("local_par_set") == cgiFormSuccess))  
    {
    	  
    				cgiFormStringSpaceNeeded("local_ip", &len);
    				cgiFormStringNoNewlines("local_ip", ip, 20);
    				ret=check_input_ip(ip,len);
    				
    				if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的本地ip地址!</td></tr>");	
	    				goto END;
    				}
    				
    				cgiFormStringSpaceNeeded("local_netmask", &len);
    				cgiFormStringNoNewlines("local_netmask", netmask_set, 20);
    				ret=check_input_ip(netmask_set,len);
    				
    				if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的子网掩码!</td></tr>");	
	    				goto END;
    				}
    				
    				cgiFormStringSpaceNeeded("local_gateway", &len);
    				cgiFormStringNoNewlines("local_gateway", gateway_set, 20);
    				ret=check_input_ip(gateway_set,len);
    				
    				if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的网关!</td></tr>");	
	    				goto END;
    				}
    				
    				
    				cgiFormStringSpaceNeeded("local_macadd", &len);
    				cgiFormStringNoNewlines("local_macadd", macaddr_set, 20);
    				ret=check_input_macadd(macaddr_set,len);
    				
    				if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的物理地址!</td></tr>");	
	    				goto END;
    				}
  
						if((fp_mac=fopen("/mnt/nand1-1/config/macadress","w"))==NULL)
							{
							//printf("can not open command passwd the file.\n");
							return -1;
							}
									
						fprintf(fp_mac,"#!/bin/sh\n");	
						fprintf(fp_mac,"/sbin/ifconfig eth0 %s netmask %s broadcast 192.168.1.255\n",ip,netmask_set);							
						fprintf(fp_mac,"/sbin/ifconfig eth0 hw ether %s\n",macaddr_set);
						
						fclose(fp_mac);
						
						
						
						cgiFormStringSpaceNeeded("cam_ipaddr", &len);
    				cgiFormStringNoNewlines("cam_ipaddr", cam_ip, 20);

						ret=check_input_ip(cam_ip,len);
						
    				if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的摄像头ip地址!</td></tr>");	
	    				goto END;
    				}
    				
    				write_file ( "/mnt/nand1-1/config/cam_ipadd",cam_ip);
						strcpy(cam_ip_now,cam_ip);
						
						write_file ( "/mnt/nand1-1/config/ipadress",ip);
						write_file ( "/mnt/nand1-1/config/netmask",netmask_set);
						write_file ( "/mnt/nand1-1/config/gateway",gateway_set);
						write_file ( "/mnt/nand1-1/config/macaddr",macaddr_set);
						
						ret=SetipAddr("eth0",ip,netmask_set,gateway_set);	
						
						if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的ip!</td></tr>");	
	    				goto END;
    				}			    				
						ether_atoe(macaddr_set,mac_add);
						
    				
						ret=set_mac_addr("eth0",mac_add);
						if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的物理地址!</td></tr>");	
	    				goto END;
    				}	
  					
  					
						inet_setroute("192.168.0.0","255.255.0.0");
						inet_setroute("224.0.0.0","224.0.0.0");
						
						strcpy(ip_now,ip);
						strcpy(netmask_now,netmask_set);
						strcpy(gateway_now,gateway_set);
						strcpy(macaddr_now,macaddr_set);
						
						sprintf(tmp_buffer,"kzq_ip<%s>",cam_ip);
						send_massge(tmp_buffer);
						
       
    } 
    
      
  
  
  
    END:
    ShowIndex();  
    fprintf(cgiOut, "</BODY></HTML>\n");  
   // fprintf(cgiOut, "</HTML>\n"); 
    return 0;  
}  


#include <stdio.h>  
#include "cgic.h"  
#include <string.h>  
#include <stdlib.h>  
#include <sys/types.h>    
#include <unistd.h>    
#include <fcntl.h>
#include<linux/reboot.h>  

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/if_arp.h>

#include <netdb.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <net/route.h>

#include <linux/rtc.h>

#include   <termios.h>
#include   <errno.h>
#include   <malloc.h>
  
#define SOFILENUM   10  
#define SOFILELEN   20  
  
char ip[20]={0}; 
char ip_now[20]={0}; 
char netmask_set[20]={0}; 
char netmask_now[20]={0}; 
char gateway_set[20]={0}; 
char gateway_now[20]={0}; 
char macaddr_set[20]={0}; 
char macaddr_now[20]={0}; 
 
char icam_ip[20]={0}; 
char icam_ip_now[20]={0}; 

char ocam_ip[20]={0}; 
char ocam_ip2[20]={0}; 

char ocam_ip_now[20]={0}; 
 char ocam_ip_now2[20]={0}; 
 
char Version[20]={0};
char adev_adress[20]={0};  
char ddev_adress[20]={0};  
char iter_num[8];
char iter_num_now[8];

char oter_num[8];
char oter_num_now[8];

char xq_name[32];
char xq_name_now[32];
char kzqld_num[8];
char kzqld_num_now[8];
char kzqdy_num[8];
char kzqdy_num_now[8];

char pt_ip[32];
char pt_ip_now[32];

char pt_port[32];
char pt_port_now[32];

char mkj_ipadd[32];
char mkj_ipadd_now[32];

char dev_id[32];
char dev_id_now[32];


char TotleSoNum=0;  


//char check_address[50][10];

char *def_type[] = {
	"Red",
	"Green",
	"Blue"
};


FILE *fd_debug; 

enum ErrLog  
{  
    ErrSucceed,  
    ErrOpenField,  
    ErrNoFile  
};  

/* 
Find the '=' pos and get the config dat 
*/  

#define ETHER_ADDR_LEN    6
#define UP    1
#define DOWN    0

#define PORT 6666

#define FIFO_NAME "/tmp/fifo"
#define FOFI_NAME "/tmp/fofi"

int set_device(int ,char *,char* ,int);
int check_input_num(char *,int );
int write_file ( char* file,char* date )
{
//int fd = open (journal_filename, O_WRONLY | O_CREAT | O_APPEND, 0660);
FILE* fp_w;

int fd_w;
if ( ( fp_w=fopen ( file,"w" ) ) ==NULL )
{
	//printf ( "can not open the file. write_file\n" );
	return -1;
}
fd_w = fileno(fp_w);
fputs ( date,fp_w );
fputs ( "\n",fp_w );

fflush(fp_w);
fsync (fd_w);
fclose ( fp_w );



}


int read_file ( char* file,char* date )
{
	FILE* fp_r ;
	if ( ( fp_r=fopen ( file,"r" ) ) ==NULL )
	{
		//printf ( "can not open the file. read_file\n" );
		return -1;
	}
	fgets ( date,20,fp_r );//jimmy ??? fgets ( date,sizeof(date),fp_r );
	fclose ( fp_r );
	return 0;
}


int get_mac_addr(char *ifname, char *mac)
{
    int fd, rtn;
    struct ifreq ifr;
    
    if( !ifname || !mac ) {
        return -1;
    }
    fd = socket(AF_INET, SOCK_DGRAM, 0 );
    if ( fd < 0 ) {
        perror("socket");
           return -1;
    }
    ifr.ifr_addr.sa_family = AF_INET;    
    strncpy(ifr.ifr_name, (const char *)ifname, IFNAMSIZ - 1 );

    if ( (rtn = ioctl(fd, SIOCGIFHWADDR, &ifr) ) == 0 )
        memcpy(    mac, (unsigned char *)ifr.ifr_hwaddr.sa_data, 6);
    close(fd);
    return rtn;
}

int set_mac_addr(char *ifname, char *mac)
{
    int fd, rtn;
    struct ifreq ifr;

    if( !ifname || !mac ) {
        return -1;
    }
    fd = socket(AF_INET, SOCK_DGRAM, 0 );
    if ( fd < 0 ) {
        perror("socket");
        return -1;
    }
    ifr.ifr_addr.sa_family = ARPHRD_ETHER;
    strncpy(ifr.ifr_name, (const char *)ifname, IFNAMSIZ - 1 );
    memcpy((unsigned char *)ifr.ifr_hwaddr.sa_data, mac, 6);
    
    if ( (rtn = ioctl(fd, SIOCSIFHWADDR, &ifr) ) != 0 ){
        perror("SIOCSIFHWADDR");
    }
    close(fd);
    return rtn;
}

int if_updown(char *ifname, int flag)
{
    int fd, rtn;
    struct ifreq ifr;        

    if (!ifname) {
        return -1;
    }

    fd = socket(AF_INET, SOCK_DGRAM, 0 );
    if ( fd < 0 ) {
        perror("socket");
        return -1;
    }
    
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, (const char *)ifname, IFNAMSIZ - 1 );

    if ( (rtn = ioctl(fd, SIOCGIFFLAGS, &ifr) ) == 0 ) {
        if ( flag == DOWN )
            ifr.ifr_flags &= ~IFF_UP;
        else if ( flag == UP ) 
            ifr.ifr_flags |= IFF_UP;
        
    }

    if ( (rtn = ioctl(fd, SIOCSIFFLAGS, &ifr) ) != 0) {
        perror("SIOCSIFFLAGS");
    }

    close(fd);

    return rtn;
}

/*
 * Convert Ethernet address string representation to binary data
 * @param    a    string in xx:xx:xx:xx:xx:xx notation
 * @param    e    binary data
 * @return    TRUE if conversion was successful and FALSE otherwise
 */
int
ether_atoe(const char *a, unsigned char *e)
{
    char *c = (char *) a;
    int i = 0;

    memset(e, 0, ETHER_ADDR_LEN);
    for (;;) {
        e[i++] = (unsigned char) strtoul(c, &c, 16);
        if (!*c++ || i == ETHER_ADDR_LEN)
            break;
    }
    return (i == ETHER_ADDR_LEN);
}


/*
 * Convert Ethernet address binary data to string representation
 * @param    e    binary data
 * @param    a    string in xx:xx:xx:xx:xx:xx notation
 * @return    a
 */
char *
ether_etoa(const unsigned char *e, char *a)
{
    char *c = a;
    int i;

    for (i = 0; i < ETHER_ADDR_LEN; i++) {
        if (i)
            *c++ = ':';
        c += sprintf(c, "%02X", e[i] & 0xff);
    }
    return a;
}



int SetipAddr(char *ifname, char *Ipaddr, char *mask,char *gateway)  
	{  
		int fd;  
		//int rc;  
		struct ifreq ifr;	
		struct sockaddr_in *sin;  

	  
		fd = socket(AF_INET, SOCK_DGRAM, 0);  
		if(fd < 0)	
		{  
				perror("socket	 error");		
				return -1;		 
		}  
		memset(&ifr,0,sizeof(ifr));   
		strcpy(ifr.ifr_name,ifname);   
		sin = (struct sockaddr_in*)&ifr.ifr_addr;		
		sin->sin_family = AF_INET;		 
		//IP地址  
		if(inet_aton(Ipaddr,&(sin->sin_addr)) < 0)	   
		{		
			perror("inet_aton	error");	   
			return -2;		 
		}	   
	  
		if(ioctl(fd,SIOCSIFADDR,&ifr) < 0)	   
		{		
			perror("ioctl	SIOCSIFADDR   error");		 
			return -3;		 
		}  
		//子网掩码	
		if(inet_aton(mask,&(sin->sin_addr)) < 0)	 
		{		
			perror("inet_pton	error");	   
			return -4;		 
		}	   
		if(ioctl(fd, SIOCSIFNETMASK, &ifr) < 0)  
		{  
			perror("ioctl");  
			return -5;	
		}  
		//网关	
		/*
		memset(&rt, 0, sizeof(struct rtentry));  
		memset(sin, 0, sizeof(struct sockaddr_in));  
		sin->sin_family = AF_INET;	
		sin->sin_port = 0;	
		if(inet_aton(gateway, &sin->sin_addr)<0)  
		{  
		   DEBUG ( "inet_aton error\n" );	
		   return -6;  
		}  
		memcpy ( &rt.rt_gateway, sin, sizeof(struct sockaddr_in));	
		((struct sockaddr_in *)&rt.rt_dst)->sin_family=AF_INET;  
		((struct sockaddr_in *)&rt.rt_genmask)->sin_family=AF_INET;  
		rt.rt_flags = RTF_GATEWAY;	
		if (ioctl(fd, SIOCADDRT, &rt)<0)  
		{  
			//zError( "ioctl(SIOCADDRT) error in set_default_route\n");  
			close(fd);	
			return -1;	
		}  */
		close(fd);	
		return 1;  
	}
	
	
	


  
  

void ReadTandaConf(void)  
{  
    FILE *fd;  
    char StrLine[64];    
    char ptr[20];  
    int i=0;  
    
   	read_file ( "/mnt/nand1-1/config/ipadress",ip_now);
		read_file ( "/mnt/nand1-1/config/netmask",netmask_now);
		read_file ( "/mnt/nand1-1/config/gateway",gateway_now);
		read_file ( "/mnt/nand1-1/config/macaddr",macaddr_now);
		read_file ( "/mnt/nand1-1/config/ocam_ipadd",ocam_ip_now);
		read_file ( "/mnt/nand1-1/config/ocam_ipadd2",ocam_ip_now2);
		read_file ( "/mnt/nand1-1/config/icam_ipadd",icam_ip_now);
		read_file ( "/mnt/nand1-1/config/iter_tm",iter_num_now);
		read_file ( "/mnt/nand1-1/config/oter_tm",oter_num_now);
		
		read_file ( "/mnt/nand1-1/config/xqcname",xq_name_now);
		read_file ( "/mnt/nand1-1/config/kzqldcnum",kzqld_num_now);
		read_file ( "/mnt/nand1-1/config/kzqdycnum",kzqdy_num_now);
		
		read_file ( "/mnt/nand1-1/config/ptcip",pt_ip_now);
		read_file ( "/mnt/nand1-1/config/ptcport",pt_port_now);
		read_file ( "/mnt/nand1-1/config/mkj_ipaddress",mkj_ipadd_now);
    read_file ( "/mnt/nand1-1/config/dev_id",dev_id_now);                    
      
}  

char *colors[] = {
	"1","2","3","4" ,"5" ,"6" ,"7"
};

char *colors1[3] = {
	"1","2","3"
};

char *sele_num[] = {
	"1", "2" ,"3" ,"4" ,"5" ,"6" ,"7" ,"8", "9" ,"10",
"11", "12" ,"13" ,"14", "15" ,"16" "17" "18" "19" "20",
"21", "22" ,"23", "24", "25", "26" ,"27" ,"28", "29" ,"30",
"31", "32", "33" ,"34", "35" ,"36", "37" ,"38", "39", "40",
"41", "42", "43", "44", "45", "46" ,"47" ,"48" ,"49" ,"50",
"51" ,"52" ,"53", "54" ,"55" ,"56", "57" ,"58" ,"59" ,"60",
"61" ,"62" ,"63", "64", "65" ,"66", "67", "68", "69", "70",
"71", "72", "73" ,"74", "75", "76", "77", "78", "79", "80",
"81" ,"82", "83" ,"84", "85", "86" ,"87", "88" ,"89", "90",
"91", "92", "93", "94", "95" ,"96", "97", "98", "99"
};


int Alarn_Clean()
{
	char tmp_buf[32];
	FILE*pFile;
	char cpm_adder[4];
	int i;
	char ch;
	
	memset(tmp_buf, 0, sizeof(tmp_buf));

if((pFile=fopen("/mnt/nand1-1/config/fa_num","r+"))==NULL)
		{
		printf("can not open the file.\n");
		return -1;
		}
		
		while(!feof(pFile))
			{	
				 memset(tmp_buf  , 0 , sizeof(tmp_buf) );
					while(1)
					{
							ch = fgetc(pFile);
							if(ch=='#' || ch=='\n' || ch=='\0')
								break;
					}
					if(ch=='#')
						{
						fputc('1',pFile);
					}
					fgets( tmp_buf ,  sizeof(tmp_buf)	, pFile );
				 	printf("tmp_buf is %s\n",tmp_buf);
	
			

   		}
   		fclose(pFile);
}

int Set_mkjip()
{
			
			int len,ret;
			char send_mag[64];
			
			cgiFormStringSpaceNeeded("mkj_ip", &len);
    	cgiFormStringNoNewlines("mkj_ip", mkj_ipadd, 32);

  	  ret=check_input_ip(mkj_ipadd,len);
  				
  				if(ret<0)
  				{
						fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
    				请输入正确的门口机ip地址!</td></tr>");	
    				return -1;
  				}
  
  			write_file ( "/mnt/nand1-1/config/mkj_ipaddress",mkj_ipadd);
	  		sprintf(send_mag,"<command>set_mkjip</command><data>%s#",mkj_ipadd);
	  		send_massge(send_mag);
  		
  			ret=rec_massge();
				if(ret==0)
				{
					fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
  				添加成功</td></tr>");
				}
				else
				{
					fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
  				添加失败</td></tr>");
				}
						
			//sprintf(fa_adress, "%s%s%s%c%c%.2d%.2d#1001",fq_num, ld_num, dy_num,csh_num[2],csh_num[3], atoi(colors1[Choicedt]), atoi(colors[Choicef]));	
}
  
  
  int Set_devid()
{
			
			int len,ret;
			char send_mag[64];
			
			cgiFormStringSpaceNeeded("dev_id", &len);
    	cgiFormStringNoNewlines("dev_id", dev_id, 32);

  	    if((len-1)!=7)
			  	{
			  		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
				    				请输入正确的设备id！</td></tr>");	
			  		return -1;
			  	}
			  else
			  	{
			  		ret=check_input_num(dev_id,len);
			  		if(ret==-1)
			  			{
			  				fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
				    				请输入正确的设备id！</td></tr>");	
			  				return -1;
			  			}
			  	}
  
  			write_file ( "/mnt/nand1-1/config/dev_id",dev_id);
	  		sprintf(send_mag,"<command>set_devid</command><data>%s#",dev_id);
	  		send_massge(send_mag);
  		
  			ret=rec_massge();
				if(ret==0)
				{
					fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
  				添加成功</td></tr>");
				}
				else
				{
					fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
  				添加失败</td></tr>");
				}
						
			//sprintf(fa_adress, "%s%s%s%c%c%.2d%.2d#1001",fq_num, ld_num, dy_num,csh_num[2],csh_num[3], atoi(colors1[Choicedt]), atoi(colors[Choicef]));	
}



  int set_kzq()
  		    {
  		    	int len,ret;
  		    	char send_mag[64];
  		    	
					cgiFormStringSpaceNeeded("xq_name", &len);
  				cgiFormStringNoNewlines("xq_name", xq_name, 32);
		  
				  if((len-1)>10 || (len-1)==0)
				  	{
				  		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
					    				请输入正确的小区名称！</td></tr>");	
				  		return -1;
				  	}
			  	
			  	cgiFormStringSpaceNeeded("kzqld_num", &len);
		  		cgiFormStringNoNewlines("kzqld_num", kzqld_num, 8);
		  				
			 	  if((len-1)!=2)
			  	{
			  		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
				    				请输入正确的楼栋号码！</td></tr>");	
			  		return -1;
			  	}
			  else
			  	{
			  		ret=check_input_num(kzqld_num,len);
			  		if(ret==-1)
			  			{
			  				fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
				    				请输入正确的楼栋号码!</td></tr>");	
			  				return -1;
			  			}
			  	}
			  	
			  	
			  	cgiFormStringSpaceNeeded("kzqdy_num", &len);
		  		cgiFormStringNoNewlines("kzqdy_num", kzqdy_num, 8);
		  				
			 	  if((len-1)!=2)
			  	{
			  		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
				    				请输入正确的单元号码！</td></tr>");	
			  		return -1;
			  	}
			  else
			  	{
			  		ret=check_input_num(kzqdy_num,len);
			  		if(ret==-1)
			  			{
			  				fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
				    				请输入正确的单元号码!</td></tr>");	
			  				return -1;
			  			}
			  	}
			  	
			  	write_file ( "/mnt/nand1-1/config/xqcname",xq_name);
					write_file ( "/mnt/nand1-1/config/kzqdycnum",kzqdy_num);
					write_file ( "/mnt/nand1-1/config/kzqldcnum",kzqld_num);
					
			  	sprintf(send_mag,"<command>set_kzq</command>");
			  	send_massge(send_mag);
  		
					ret=rec_massge();
					if(ret==0)
					{
						fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
						设置成功</td></tr>");
					}
					else
					{
						fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
						设置失败</td></tr>");
					}
			  	
    	 }
    	 
    	 
  int set_pt()
  		    {
  		    int len,ret;
  		    char send_mag[64];
  		    
					cgiFormStringSpaceNeeded("pt_ip", &len);
  				cgiFormStringNoNewlines("pt_ip", pt_ip, 32);
		  
				  ret=check_input_ip(pt_ip,len);
  				if(ret<0)
  				{
						fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
    				请输入正确的平台ip地址!</td></tr>");	
    				return -1;
  				}
			  	
			  	cgiFormStringSpaceNeeded("pt_port", &len);
		  		cgiFormStringNoNewlines("pt_port", pt_port, 8);
		  				
			 	  if((len-1)==0 || (len-1)>5)
			  	{
			  		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
				    				请输入正确的平台端口号！</td></tr>");	
			  		return -1;
			  	}
			  else
			  	{
			  		ret=check_input_num(pt_port,len);
			  		if(ret==-1)
			  			{
			  				fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
				    				请输入正确的平台端口号!</td></tr>");	
			  				return -1;
			  			}
			  	}
			  	
			  	
			  	
			  	write_file ( "/mnt/nand1-1/config/ptcip",pt_ip);
					write_file ( "/mnt/nand1-1/config/ptcport",pt_port);
					
			  	sprintf(send_mag,"<command>set_pt</command>");
			  	send_massge(send_mag);
  		
					ret=rec_massge();
					if(ret==0)
					{
						fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
						设置成功</td></tr>");
					}
					else
					{
						fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
						设置失败</td></tr>");
					}
			  	
    	 }
   
int SetSystemTime ( char* dt )
{
	struct rtc_time tm;
	struct tm _tm;
	struct tm* tm24h = NULL;
	struct timeval tv;
	time_t timep;
	int  fd_time = 0;
	struct rtc_time rtc_tm;
	int retval;

	sscanf ( dt, "%4d%2d%2d%2d%2d", &tm.tm_year,
	         &tm.tm_mon, &tm.tm_mday,&tm.tm_hour,
	         &tm.tm_min );

	printf ( "%d,%d,%d,%d,%d\n", tm.tm_year,
	        tm.tm_mon, tm.tm_mday,tm.tm_hour,
	        tm.tm_min );


	_tm.tm_sec = 0;
	_tm.tm_min = tm.tm_min;
	_tm.tm_hour = tm.tm_hour;
	_tm.tm_mday = tm.tm_mday;
	_tm.tm_mon = tm.tm_mon - 1;
	_tm.tm_year = tm.tm_year - 1900;

	timep = mktime ( &_tm );
	tv.tv_sec = timep;
	tv.tv_usec = 0;
	if ( settimeofday ( &tv, ( struct timezone* ) 0 ) < 0 )
	{
		printf ( "Set system datatime error!/n" );
		return -1;
	}
	printf ( "8 houer befor \n" );
	timep -= 8*60*60;
	tm24h = localtime ( &timep );
	printf ( "8 houer befor is：%dyear %02dmount %02dday,%02dhouer %02dmin %02ds\n",
	        tm24h->tm_year+1900,tm24h->tm_mon,tm24h->tm_mday,
	        tm24h->tm_hour,tm24h->tm_min,tm24h->tm_sec );


	fd_time = open ( "/dev/rtc", O_RDONLY );

	if ( fd_time<0 )
	{
		printf ( "\topen /dev/rtc faild!!!\n" );
		printf ( "Try tp open /dev/rtc0\n" );
		fd_time = open ( "/dev/rtc0", O_RDONLY );

		if ( fd_time<0 )
		{
			printf ( "\topen /dev/rtc0 faild!!!\n" );
			printf ( "Try tp open /dev/misc/rtc\n" );
			fd_time = open ( "/dev/misc/rtc", O_RDONLY );

			if ( fd_time<0 )
			{
				printf ( "\topen /dev/misc/rtc faild!!!\n" );
				return -1;
			}
			else
			{
				printf ( "\trtc is /dev/misc/rtc\n" );
			}
		}
		else
		{
			printf ( "\trtc is /dev/rtc0\n" );
		}
	}
	else
	{
		printf ( "\trtc is /dev/rtc\n" );
	}

	printf ( "Input the adjust time1234:\n" );

	rtc_tm.tm_year = tm24h->tm_year;
	rtc_tm.tm_mon = tm24h->tm_mon ;
	rtc_tm.tm_mday = tm24h->tm_mday;
	rtc_tm.tm_hour = tm24h->tm_hour;
	rtc_tm.tm_min = tm24h->tm_min;
	rtc_tm.tm_sec = tm24h->tm_sec;
	printf ( "Input the adjust time456:\n" );

	retval = ioctl ( fd_time, RTC_SET_TIME, &rtc_tm );
	printf ( "Input the adjust time789:\n" );
	if ( retval <0 )
	{
		printf ( "ioctl RTC_SET_TIME	faild!!!\n" );
		return -1;
	}


	printf ( "Adjust current RTC time as: %04d-%02d-%02d %02d:%02d:%02d\n\n",
	        rtc_tm.tm_year + 1900,
	        rtc_tm.tm_mon + 1,
	        rtc_tm.tm_mday,
	        rtc_tm.tm_hour,
	        rtc_tm.tm_min,
	        rtc_tm.tm_sec );
	printf ( "Adjust current RTC time test OK!\n" );


	close ( fd_time );


	return 0;
}
 	 
int set_con_time()
{
	int len,ret;
	char contime_buf[64];
	char send_mag[64];
	cgiFormStringSpaceNeeded("con_time", &len);
	cgiFormStringNoNewlines("con_time", contime_buf, 64);
	if((len-1)==12)
		{
			//SetSystemTime ( contime_buf );
			sprintf(send_mag,"<command>set_time</command><data>%s</data>",contime_buf);
			send_massge(send_mag);
  		  
			ret=rec_massge();
		}
		else
		{
				fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的时间!</td></tr>");	
  			return -1;
		}
}
    	 
int Set_fanum(int manner) {
	int Choicef=0,Choicel=0,Choiced=0,Choicec=0,Choicedt=0;
	char fa_adress[32];
	char fq_num[16];
	char ld_num[16];
	char dy_num[16];
	char csh_num[16];
	char dt_ipadd[32];
	char send_mag[64];
	int ret=0;
	
	int len;
	
				/*
				fprintf(cgiOut ,"<script type=\"text/javascript\">  ");
				fprintf(cgiOut ,"function sayHi(){    ");
				fprintf(cgiOut ," alert(\"Hi!\");   ");
				fprintf(cgiOut ,"  }  ");
				fprintf(cgiOut ,"  </script>  ");*/
	
	cgiFormStringSpaceNeeded("fq_num", &len);
  cgiFormStringNoNewlines("fq_num", fq_num, 16);
  
  if((len-1)!=3)
  	{
  		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的防区号码!</td></tr>");	
  		return -1;
  	}
  else
  	{
  		ret=check_input_num(fq_num,len);
  		if(ret==-1)
  			{
  				fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的防区号码!</td></tr>");	
  				return -1;
  			}
  	}
  
  cgiFormStringSpaceNeeded("ld_num", &len);
  cgiFormStringNoNewlines("ld_num", ld_num, 16);
  
    if((len-1)!=2)
  	{
  		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的楼栋!</td></tr>");	
  		return -1;
  	}
  else
  	{
  		ret=check_input_num(ld_num,len);
  		if(ret==-1)
  			{
			 		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
  				请输入正确的楼栋!</td></tr>");	
  				return -1;
  			}
  	}
  
  cgiFormStringSpaceNeeded("dy_num", &len);
  cgiFormStringNoNewlines("dy_num", dy_num, 16);
  
    if((len-1)!=2)
  	{
  		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的单元!</td></tr>");	
  		return -1;
  	}
  else
  	{
  		ret=check_input_num(dy_num,len);
  		if(ret==-1)
  			{
			 		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
  				请输入正确的单元!</td></tr>");	
  				return -1;
  			}
  	}
  cgiFormStringSpaceNeeded("csh_num", &len);
  cgiFormStringNoNewlines("csh_num", csh_num, 16);
  
    if((len-1)!=4)
  	{
  		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的层!</td></tr>");	
  		return -1;
  	}
  else
  	{
  		ret=check_input_num(csh_num,len);
  		if(ret==-1)
  			{
			 		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
  				请输入正确的层!</td></tr>");	
  				return -1;
  			}
  	}
  	

    				
    				
	cgiFormSelectSingle("fa_type", colors, 7, &Choicef, 0);
	//fprintf(cgiOut, "I am: %s, %d\n", colors[Choicef],Choicef);
	
	 cgiFormSelectSingle("dt_type", colors1, 3, &Choicedt, 0);
	 // fprintf(cgiOut, "I am dt_type: %s<BR>\n", colors[Choicedt]);
	  
	  cgiFormSelectSingle("dt_gztime", colors1, 3, &Choicel, 0);
	  //fprintf(cgiOut, "I am dt_gztime: %s<BR>\n", colors[Choicel]);
	  
	  
		if(atoi(colors[Choicef])==1 ||atoi(colors[Choicef])==2)
		{
			sprintf(fa_adress, "%s%s%s%c%c%.2d#1",fq_num, ld_num, dy_num, csh_num[2],csh_num[3], atoi(colors[Choicef]));	
		}
		else if(atoi(colors[Choicef])==3)
		{
				cgiFormStringSpaceNeeded("dt_ip", &len);
    		cgiFormStringNoNewlines("dt_ip", dt_ipadd, 32);
    
    		//fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
    		//		本地ip地址%s!</td></tr>",dt_ipadd);	
  	    ret=check_input_ip(dt_ipadd,len);
  				
  				if(ret<0)
  				{
						fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
    				请输入正确的楼显ip地址!</td></tr>");	
    				return -1;
  				}
  		sprintf(send_mag,"<command>find_dx</command><data>%s,%s,%s",dt_ipadd,fq_num,colors[Choicel]);
				
				
  		send_massge(send_mag);
  		  
			ret=rec_massge();

				
			if(ret==0)
				{
				fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
				添加成功</td></tr>");
				sprintf(fa_adress, "%s%s%s%c%c%.2d%.2d#1001",fq_num, ld_num, dy_num,csh_num[2],csh_num[3], atoi(colors1[Choicedt]), atoi(colors[Choicef]));	
				//return -1;
			}
			else
			{
				fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
				添加失败</td></tr>");
				//return -1;
			}
						
						
			
		}
		else
		{
			sprintf(fa_adress, "%s%s%s%s%.2d#1",fq_num, ld_num, dy_num, csh_num, atoi(colors[Choicef]));	
		}
		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请%s</td></tr>",fa_adress);	
	    				
	   set_device(manner,fa_adress,"fa_num",3);
	    				
	/*
	cgiFormSelectSingle("ld_num", sele_num, 99, &Choicel, 0);
	fprintf(cgiOut, "I am ld_num: %s, %d\n", sele_num[Choicel],Choicel);
	
		cgiFormSelectSingle("dy_num", sele_num, 99, &Choiced, 0);
	fprintf(cgiOut, "I am dy_num: %s, %d\n", sele_num[Choiced],Choiced);
	
		cgiFormSelectSingle("csh_num", sele_num, 99, &Choicec, 0);
	fprintf(cgiOut, "I am csh_num: %s, %d\n", sele_num[Choicec],Choicec);
	
		if(atoi(colors[Choicef])==3)
			{
		cgiFormSelectSingle("dt_type", colors1, 3, &Choicedt, 0);
	  fprintf(cgiOut, "I am dt_type: %s<BR>\n", colors[Choicedt]);
		sprintf(fa_adress, "%s%.2d%.2d%.2d%.2d%.2d",fq_num,atoi(sele_num[Choicel]), atoi(sele_num[Choiced])
		, atoi(sele_num[Choicec]), atoi(colors1[Choicedt]), atoi(colors[Choicef]));
	    }
	    else
	    	{
	    		fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请%d</td></tr>",atoi(sele_num[Choicel]));	
	    				
	    		sprintf(fa_adress, "%s%.2d%.2d%.2d%.2d",fq_num, atoi(sele_num[Choicel]), atoi(sele_num[Choiced])
		, atoi(sele_num[Choicec]), atoi(colors[Choicef]));	
	   }
	   
	 fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请%s</td></tr>",fa_adress);	*/
		//set_device(1,fa_adress,"fa_num",3);
}	 

  
void ShowIndex(void)  
{  
    FILE *fd;  
    char StrLine[1024];  
    int i=0;  
      
    ReadTandaConf();  
    
    //fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp//&nbsp&nbsp&nbsp&nbsp\门牌号修改为:</td><td>%s</td></tr>","123");
			   	
			  
	  fprintf(cgiOut, "<!-- 2.0: multipart/form-data is required for file uploads. -->");  
    fprintf(cgiOut, "<form method=\"POST\" enctype=\"multipart/form-data\" ");  
    fprintf(cgiOut, "   action=\"");  
    cgiValueEscape(cgiScriptName);  
    fprintf(cgiOut, "\">\n");  
    fprintf(cgiOut, "<p>\n"); 
    
    		  
			fprintf(cgiOut ,"<p align=\"center\">");    	
			fprintf(cgiOut ,"<input type=\"button\" value=\"本地设置\" style=\"font-size:20px;background-color: #0000ff;color:white\" onclick=\"window.location = 'ent_recon_set.cgi'\" />");   	
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<input type=\"button\" value=\"实时状态\" style=\"font-size:20px;background-color: #0000ff;color:white\" onclick=\"window.location = 'face_web.cgi'\" />");   	
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<input type=\"button\" value=\"升级\" style=\"font-size:20px;background-color: #0000ff;color:white\" onclick=\"window.location = 'updata.cgi'\" />");   	
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"</p>");    
		

				  
			
			fprintf(cgiOut ,"<hr width=\"100%%\" size=\"10\" color=\"#0000ff\" align=\"center\">");
			 
			fprintf(cgiOut ,"<p>");   
			fprintf(cgiOut ,"<br/>");   
			fprintf(cgiOut ,"<br/>");      
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"本机ip地址：<input value=\"%s\" type=\"text\" name=\"local_ip\" id=\"local_ip\" />",ip_now); 
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"本机子网掩码：<input value=\"%s\" type=\"text\" name=\"local_netmask\" id=\"local_netmask\" />",netmask_now); 
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"本机默认网关：<input value=\"%s\" type=\"text\" name=\"local_gateway\" id=\"local_gateway\" />",gateway_now); 
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"本机物理地址：<input value=\"%s\" type=\"text\" name=\"local_macadd\" id=\"local_macadd\" />",macaddr_now); 
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\">");  
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"local_par_set\" value=\"确定\" onclick=\"javaScript:alert('请求已发送')\">");  
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"clean_set\" value=\"取消\" >");   
			fprintf(cgiOut ,"</p>");  
			
			
			
			
			fprintf(cgiOut ,"<p>");   
			fprintf(cgiOut ,"<br/>");   
			fprintf(cgiOut ,"<br/>");      
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"小区名字：<input value=\"%s\" type=\"text\" name=\"xq_name\" id=\"xq_name\" />",xq_name_now); 
			fprintf(cgiOut ,"</p>");  
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"控制器所在楼栋号：<input value=\"%s\" type=\"text\" name=\"kzqld_num\" id=\"kzqld_num\" />",kzqld_num_now); 
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"控制器所在单元号：<input value=\"%s\" type=\"text\" name=\"kzqdy_num\" id=\"kzqdy_num\" />",kzqdy_num_now); 
			fprintf(cgiOut ,"</p>"); 

			
			fprintf(cgiOut ,"<p align=\"center\">");  
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"kzq_set\" value=\"确定\" onclick=\"javaScript:alert('请求已发送')\">");  
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"kzq_set\" value=\"取消\" >");   
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p>");   
			fprintf(cgiOut ,"<br/>");   
			fprintf(cgiOut ,"<br/>");      
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"控制器时间设定（年月日时分）：<input value=\"201803120000\" type=\"text\" name=\"con_time\" id=\"con_time\" />"); 
			fprintf(cgiOut ,"</p>"); 

			
			fprintf(cgiOut ,"<p align=\"center\">");  
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"contime_set\" value=\"确定\" onclick=\"javaScript:alert('请求已发送')\">");  
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"contime_cle\" value=\"取消\" >");   
			fprintf(cgiOut ,"</p>"); 
			
			
			
			
			fprintf(cgiOut ,"<p>");   
			fprintf(cgiOut ,"<br/>");   
			fprintf(cgiOut ,"<br/>");      
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"平台IP地址：<input value=\"%s\" type=\"text\" name=\"pt_ip\" id=\"pt_ip\" />",pt_ip_now); 
			fprintf(cgiOut ,"</p>");  
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"平台端口号码：<input value=\"%s\" type=\"text\" name=\"pt_port\" id=\"pt_port\" />",pt_port_now); 
			fprintf(cgiOut ,"</p>"); 
			
			
			fprintf(cgiOut ,"<p align=\"center\">");  
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"pt_set\" value=\"确定\" onclick=\"javaScript:alert('请求已发送')\">");  
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"pt_set\" value=\"取消\" >");   
			fprintf(cgiOut ,"</p>"); 
			
			
			
			fprintf(cgiOut ,"<p>");   
			fprintf(cgiOut ,"<br/>");   
			fprintf(cgiOut ,"<br/>");      
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"进门摄像机ip地址：<input value=\"%s\" type=\"text\" name=\"icam_ipaddr\" id=\"icam_ipaddr\" />",icam_ip_now); 
			fprintf(cgiOut ,"</p>");  
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"获取图像间隔时间：<input value=\"%s\" type=\"text\" name=\"iint_time\" id=\"iint_time\" />",iter_num_now); 
			fprintf(cgiOut ,"</p>");  
			
			fprintf(cgiOut ,"<p align=\"center\">");  
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"icam_par_set\" value=\"确定\" onclick=\"javaScript:alert('请求已发送')\">");  
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"iclean_set\" value=\"取消\" >");   
			fprintf(cgiOut ,"</p>");  
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"出门摄像机1 ip地址：<input value=\"%s\" type=\"text\" name=\"ocam_ipaddr\" id=\"ocam_ipaddr\" />",ocam_ip_now); 
			fprintf(cgiOut ,"</p>");  
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"出门摄像机2 ip地址：<input value=\"%s\" type=\"text\" name=\"ocam_ipaddr2\" id=\"ocam_ipaddr2\" />",ocam_ip_now2); 
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"获取图像间隔时间：<input value=\"%s\" type=\"text\" name=\"oint_time\" id=\"oint_time\" />",oter_num_now); 
			fprintf(cgiOut ,"</p>");  
			

			fprintf(cgiOut ,"<p align=\"center\">");  
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"ocam_par_set\" value=\"确定\" onclick=\"javaScript:alert('请求已发送')\">");  
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"oclean_set\" value=\"取消\" >");   
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p>");   
			fprintf(cgiOut ,"<br/>");   
			fprintf(cgiOut ,"<br/>");      
			fprintf(cgiOut ,"</p>");
			
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"防区号码：<input value=\"001\" type=\"text\" name=\"fq_num\" id=\"fq_num\" />"); 
			fprintf(cgiOut ,"</p>");  
			
			/*
			fprintf(cgiOut ,"<p align=\"center\">"); 
			fprintf(cgiOut ,"<select align=\"center\" name=\"select_type\" id=\"select_type\" class=\"xla_k\" style=\"margin-top:10px;\">");  
			fprintf(cgiOut ,"<option>-类型-</option>");  
			fprintf(cgiOut ,"<option value=\"1\" selected>烟感</option>"); 
			fprintf(cgiOut ,"<option value=\"2\">门磁</option>"); 
			fprintf(cgiOut ,"</select>");
			fprintf(cgiOut ,"</p>");
			*/ 
			fprintf(cgiOut ,"<p align=\"center\">"); 
			fprintf(cgiOut ,"类型"); 
			fprintf(cgiOut, "<select name=\"fa_type\" class=\"xla_k\" style=\"margin-top:10px;\">");
			fprintf(cgiOut, "<option value=\"1\">烟感</option>");
			fprintf(cgiOut, "<option value=\"2\">门磁</option>");
			fprintf(cgiOut, "<option value=\"3\">电梯</option>");
			fprintf(cgiOut, "<option value=\"4\">关爱烟感</option>");
			fprintf(cgiOut, "<option value=\"5\">关爱温感</option>");
			fprintf(cgiOut, "<option value=\"6\">关爱燃气</option>");
			fprintf(cgiOut, "<option value=\"7\">关爱红外</option>");
			fprintf(cgiOut, "</select>\n");
			
			fprintf(cgiOut ,"</p>");
			
			fprintf(cgiOut ,"<p align=\"center\">"); 
			
			fprintf(cgiOut ,"电梯类型"); 
			fprintf(cgiOut, "<select name=\"dt_type\" class=\"xla_k\" style=\"margin-top:10px;\">");
			fprintf(cgiOut, "<option value=\"1\">客梯</option>");
			fprintf(cgiOut, "<option value=\"2\">货梯</option>");
			fprintf(cgiOut, "<option value=\"3\">火梯</option>");
			fprintf(cgiOut, "</select>\n");
			
			fprintf(cgiOut ,"楼显ip地址：<input value=\"192.168.1.19\" type=\"text\" name=\"dt_ip\" id=\"dt_ip\" />"); 
			
			
			fprintf(cgiOut ,"电梯故障判断时间"); 
			fprintf(cgiOut, "<select name=\"dt_gztime\" class=\"xla_k\" style=\"margin-top:10px;\">");
			fprintf(cgiOut, "<option value=\"1\">白天30分钟晚上2小时</option>");
			fprintf(cgiOut, "<option value=\"2\">白天1小时/晚上4小时</option>");
			fprintf(cgiOut, "<option value=\"3\">白天2小时/晚上6小时</option>");
			fprintf(cgiOut, "</select>\n");
			
			
			fprintf(cgiOut ,"</p>");
			
		
			
			
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"楼栋号：<input value=\"01\" type=\"text\" name=\"ld_num\" id=\"ld_num\" />"); 
			//fprintf(cgiOut ,"</p>"); 
			
			//fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"单元号：<input value=\"01\" type=\"text\" name=\"dy_num\" id=\"dy_num\" />"); 
			//fprintf(cgiOut ,"</p>"); 
			
			//fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"层/室/号：<input value=\"0101\" type=\"text\" name=\"csh_num\" id=\"csh_num\" />"); 
			fprintf(cgiOut ,"</p>"); 
			
			 
			
			fprintf(cgiOut ,"<p align=\"center\">");  
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"fq_set\" value=\"添加\" onclick=\"javaScript:alert('请求已发送')\">");  
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"fq_del\" value=\"删除\" onclick=\"javaScript:alert('请求已发送')\">");
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"fq_clean\" value=\"清除报警\" onclick=\"javaScript:alert('请求已发送')\">");      
			fprintf(cgiOut ,"</p>"); 
			
			
			fprintf(cgiOut ,"<p>");   
			fprintf(cgiOut ,"<br/>");   
			fprintf(cgiOut ,"<br/>");      
			fprintf(cgiOut ,"</p>");
			
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"门口机ip地址：<input value=\"%s\" type=\"text\" name=\"mkj_ip\" id=\"mkj_ip\" />",mkj_ipadd_now); 
			fprintf(cgiOut ,"</p>");  
			
			fprintf(cgiOut ,"<p align=\"center\">");  
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"mkjip_set\" value=\"添加\" onclick=\"javaScript:alert('请求已发送')\">");  
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"mkjip_clen\" value=\"清除报警\" onclick=\"javaScript:alert('请求已发送')\">");
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"</p>"); 
			
			
			
			fprintf(cgiOut ,"<p>");   
			fprintf(cgiOut ,"<br/>");   
			fprintf(cgiOut ,"<br/>");      
			fprintf(cgiOut ,"</p>");
			
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"门禁数据库ip地址：<input value=\"192.168.1.1\" type=\"text\" name=\"sjk_ip\" id=\"sjk_ip\" />"); 
			fprintf(cgiOut ,"</p>"); 
			
			 
			
			fprintf(cgiOut ,"<p align=\"center\">");  
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"sjk_set\" value=\"更新\" onclick=\"javaScript:alert('请求已发送')\">");  
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"sjk_clen\" value=\"取消\" onclick=\"javaScript:alert('请求已发送')\">");
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"</p>"); 
			
			fprintf(cgiOut ,"<p>");   
			fprintf(cgiOut ,"<br/>");   
			fprintf(cgiOut ,"<br/>");      
			fprintf(cgiOut ,"</p>");
			
			
			fprintf(cgiOut ,"<p align=\"center\" style=\"font-size:20px;color:Black\">"); 
			fprintf(cgiOut ,"设备id：<input value=\"%s\" type=\"text\" name=\"dev_id\" id=\"dev_id\" />",dev_id_now); 
			fprintf(cgiOut ,"</p>");  
			
			fprintf(cgiOut ,"<p align=\"center\">");  
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"dev_id_set\" value=\"确定\" onclick=\"javaScript:alert('请求已发送')\">");  
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"<tr><td></td><td align=\"left\"><input class =\"button\" input type=\"submit\"name=\"dev_id_clen\" value=\"取消\" onclick=\"javaScript:alert('请求已发送')\">");
			fprintf(cgiOut ,"&nbsp &nbsp &nbsp &nbsp"); 
			fprintf(cgiOut ,"</p>"); 
			
			
			
		  
			
}  
 
int init_debug() 
{

    if((fd_debug = fopen("/mnt/nand1-1/debug","w")) == NULL)   
    {   
        return;   
    }  
}

enum ErrLog UpLoadUpdateFile(void)  
{  
    cgiFilePtr file;  
    FILE *fd;  
    char name[1024];  
  //  char buffer[1024]; 
    char path[50];  
    char contentType[1024];  
    int size; 
    int ret=1; 
    unsigned int got;  
    char *tmp = NULL;  
    if (cgiFormFileName("updatefile", name, sizeof(name)) != cgiFormSuccess) {  
        return ErrNoFile;  
    }   
    cgiFormFileSize("updatefile", &size);  
    cgiFormFileContentType("updatefile", contentType, sizeof(contentType));  
    if (cgiFormFileOpen("updatefile", &file) != cgiFormSuccess) {  
        return ErrNoFile;  
    }  
    /*write file */  
    ret=strcmp(name,"zhq_update.bin");
    
    if(ret==0)  
    	{
    			//goto_web(0);
			    tmp=(char *)malloc(sizeof(char)*size);  
			    strcpy(path , "/mnt/nand1-1/");  
			    strcat(path, name);    
			    fd=fopen(path ,"w+");  
			    if(fd==NULL)  
			    {  
			        return ErrOpenField;  
			    }  
			    while (cgiFormFileRead(file, tmp, size, &got) ==  
			        cgiFormSuccess)  
			    {  
			        fwrite(tmp, size, sizeof(char), fd);  
			    }  
			    cgiFormFileClose(file);  
			    free(tmp);  
			    fclose(fd);  
			    return ErrSucceed;  

    	}
    	else
    		{
    		//	goto_web(1);
    		return ErrOpenField;  
    	  }
}  
  
int send_massge(char *date)
{
	int pipe_fd;
	int pipe_re;
	    
  pipe_fd=open(FIFO_NAME,O_WRONLY);
 // pipe_re=open(FOFI_NAME,O_RDONLY);
   if(pipe_fd!=-1)
    {
        write(pipe_fd,date,strlen(date));    
        close(pipe_fd); 
       // close(pipe_re);             
    }
			    
	return 0;	    
}

int rec_massge()
{
	int pipe_re;
	int pipe_fd;
	
	char buffer[32];
	//pipe_fd=open(FIFO_NAME,O_WRONLY);
  pipe_re=open(FOFI_NAME,O_RDONLY);
   if(pipe_re!=-1)
    {

        read(pipe_re,buffer,sizeof(buffer));  
        //printf("the read data is %s\n",buffer);  
        close(pipe_re);
       // close(pipe_fd);  
        
        if(( strstr(buffer, "succeed")) != NULL)
        	{
        		return 0;
        	}
        else
        	{
        		 return -1;  
        	}  
               
    }
	
	
	return -1;		    
			    
}

int inet_setroute(char *target,char *netmask)
{
    struct rtentry route;  /* route item struct */
   // char target[128] = {0};
    char gateway[128] = {0};
  // char netmask[128] = {0};
    int action=1;
    struct sockaddr_in *addr;

    int skfd;

    /* clear route struct by 0 */
    memset((char *)&route, 0x00, sizeof(route));

    /* default target is net (host)*/
    route.rt_flags = RTF_UP ;


    {
       
      //  if(!strcmp(*args, "-net"))
        {/* default is a network target */


            //strcpy(target,"224.0.0.0");
            //strcpy(target,net);
            addr = (struct sockaddr_in*) &route.rt_dst;
            addr->sin_family = AF_INET;
            addr->sin_addr.s_addr = inet_addr(target);
       
            //continue;
        }
        /*
        else if(!strcmp(*args, "-host"))
        {
            args++;
            strcpy(target, *args);
            addr = (struct sockaddr_in*) &route.rt_dst;
            addr->sin_family = AF_INET;
            addr->sin_addr.s_addr = inet_addr(target);
            route.rt_flags |= RTF_HOST;
            args++;
            //continue;
        }
        else
        {
        		printf(" -net \n");
            usage();
            return -1;
        }*/
       // if(!strcmp(*args, "netmask"))
        {/* netmask setting */
        	

           // strcpy(netmask, "224.0.0.0");
           // strcpy(netmask, mask);
            addr = (struct sockaddr_in*) &route.rt_genmask;
            addr->sin_family = AF_INET;
            addr->sin_addr.s_addr = inet_addr(netmask);
    
           // continue;
        }

        
    
       // if(!strcmp(*args, "device") || !strcmp(*args, "dev"))
        {/* device setting */
          //  args++;
            route.rt_dev = "eth0";
            //args++;
           // continue;
        }
     
        /* if you have other options, please put them in this place,
          like the options above. */
    }

	  //printf("over\n");
    /* create a socket */
    skfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(skfd < 0)
    {
        perror("socket");
        return -1;
    }

    /* tell the kernel to accept this route */
  
        if(ioctl(skfd, SIOCADDRT, &route) < 0)
        {
            perror("SIOCADDRT");
            close(skfd);
            return -1;
        }
    
    (void) close(skfd);
    return 0;
}

int goto_web(int manner)
{
				if(manner==0)
					{
	 							fprintf(cgiOut, "<HTML><HEAD>\n");  
			    		 //fprintf(cgiOut, "<meta http-equiv=\"Refresh\" content=\"0;URL=http://192.168.7.211/t1.html\">");  
			    		 fprintf(cgiOut, "<meta http-equiv=\"Refresh\" content=\"0;URL=/t1.html\">");  
			    		 fprintf(cgiOut, "</HEAD>");  
			    		 fprintf(cgiOut, "<BODY>");   		 
			    		 fprintf(cgiOut, "</BODY></HTML>\n"); 
    			} 	
    		else
    			{
    				
    					fprintf(cgiOut, "<HTML><HEAD>\n");  
			    		 //fprintf(cgiOut, "<meta http-equiv=\"Refresh\" content=\"0;URL=http://192.168.7.211/t1.html\">");  
			    		 fprintf(cgiOut, "<meta http-equiv=\"Refresh\" content=\"0;URL=/t2.html\">");  
			    		 fprintf(cgiOut, "</HEAD>");  
			    		 fprintf(cgiOut, "<BODY>");   		 
			    		 fprintf(cgiOut, "</BODY></HTML>\n"); 
    				
    			}
    			
    			
    				
}

int check_input_macadd(char *check_date,int len)
{
int point_num=0;
int i;

		for(i=0;i<len-1;i++)
		{
			
			fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				DATE IS %c </td></tr>",check_date[i]);	
	    				
			if( check_date[i]<'0' || check_date[i]>'9' )
				{
					if( check_date[i]<'a' || check_date[i]>'f' )
						{
								if( check_date[i]<'A' || check_date[i]>'F' )
							{
									if(check_date[i]!=':')
									{
											return -1;
				  				}
				  				else
									{
											point_num++;
									}
							}
					  }
				}
			
		}
		
	 	if(point_num!=5)			
	 	{
				return -1;
	 	}  
	 	else
	 	{
			return 0;
	 	}	
  	 	
  	 	return 0;
}

int check_input_num(char *check_date,int len)
{
int i;

		for(i=0;i<len-1;i++)
		{
			if( check_date[i]<'0' || check_date[i]>'9' )
				{

							return -1;
				}
			
		}
		
  	 	return 0;
}

int check_input_ip(char *check_date,int len)
{
int point_num=0;
int i;

		for(i=0;i<len-1;i++)
		{
			if( check_date[i]<'0' || check_date[i]>'9' )
				{
					if(check_date[i]!='.')
					{
							return -1;
  				}
  				else
					{
							point_num++;
					}
				}
			
		}
		
	 	if(point_num!=3)			
	 	{
				return -1;
	 	}  
	 	else
	 	{
			return 0;
	 	}	
  	 	
  	 	return 0;
}


int set_device(int manner,char *address,char* name,int num)
{

	int i=0;
	int j=0;
	int device_at=-1;
	int ret=1;
	char device_add[256][32];
	int  device_point=0;
	FILE *pFile=NULL;
	FILE *oFile=NULL;
	char filename[128];
	
	sprintf(filename,"/mnt/nand1-1/config/%s",name);

	pFile=fopen(filename,"r");
	
	
    if(pFile!=NULL)
    {
    	while(!feof(pFile))
			{	
				 memset(device_add[device_point]  , 0 , sizeof(device_add[device_point]) );
    		 fgets( device_add[device_point] ,  sizeof(device_add[device_point])  , pFile );
    		 device_point++;
    		 if(device_point>=256)
    		 	{
    		 		break;
    		 	}
   		}
   		fclose(pFile);
   		 
   		 for(i=0;i<device_point;i++)
   		 {
   		 	
   		 	for(j=0;j<num;j++)
   		 	{
   		 		if(device_add[i][j]!=address[j])
   		 			{
   		 				break;
   		 			}
   		 	}
   		 	
   		 	if(j==num)
   		 		{
   		
   		 				{
   		 				device_at=i;
   		 			//	fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbspis:</td><td>%d</td></tr>",device_at);
   		 				}
   		 		}
   		 }
   		 
    }
    
    
   

 
  		if(device_at!=-1)
  			{
   	
				oFile=fopen(filename,"w");
				// fseek(oFile,0,SEEK_END);
				for(i=0;i<device_point;i++)
				{
					if(device_at!=i)
						{
							  fputs(device_add[i],oFile);
				//	fputs("\n",oFile);
						}
						else
						{
								if(manner==1)
					  	 {
					  	 	fputs(address,oFile);
					  	 	fputs("\n",oFile);
					  	 }
						}
				}
    		fclose(oFile);
    		}
    	else
    		{
    			 if(manner==1)
				   {
				
				   	
				    oFile=fopen(filename,"a");
				   // fseek(oFile,0,SEEK_END);
				    fputs(address,oFile);
				     fputs("\n",oFile);
				    fclose(oFile);
				  	}
    		}
  
    
	return 0;
}

int cgiMain()   
{  
		char tmp_mac[100];
		char tmp_ip[100];
		char mac_add[100];
		int len;
		int len_add,len_del;
		FILE *fp_mac;
		int i;
	  int ret=0;
	  char tmp_buffer[128];
	  int TypeChoice;
	  char sjk_ipadd[32];
		//init_debug();
    /* Send the content type, letting the browser know this is HTML */  
    cgiHeaderContentType("text/html");  
    //printf("Content-type:text/html;charset=utf-8\n\n");  
    /* Top of the page */  
    fprintf(cgiOut, "<HTML><HEAD>\n");  
    fprintf(cgiOut, "<TITLE>上海跃天</TITLE>\n");  
/*
    fprintf(cgiOut , "<style>#table{border-radius:20px;border:0px;background:#87CEFA;}#table td{color:##00CED1;color:#0F0F0F0F;border:0px;}#h1{color:#CCCCCC;}");  
    fprintf(cgiOut, "input{height:30px;color:#0;border : 0;background:#F0FFFF;}.button{margin-right:10px;background:#4876FF;width:80px;border-radius:5px;color:#FAF0E6;}");  
    fprintf(cgiOut, ".button:active{background:#666666;}.file-box{ position:relative;width:340px;} .txt{ height:30px;; border:1px solid #CCCCCC; width:180px;} ");  
    fprintf(cgiOut, ".btn{ background-color:#4876FF; border-radius:5px solid #CDCDCD;height:30px; width:70px;color:#FAF0E6;} .file{ position:absolute; top:0; right:80px; height:30px; filter:alpha(opacity:0);opacity: 0;width:260px } </style>");  
 */
      
    fprintf(cgiOut, "</HEAD>");  
    
    fprintf(cgiOut, "<BODY bgcolor=\"#FFFFFF\"  onLoad=\"sayHi()\">");  
    
     if ((cgiFormSubmitClicked("local_par_set") == cgiFormSuccess))  
    {
    	  
    				cgiFormStringSpaceNeeded("local_ip", &len);
    				cgiFormStringNoNewlines("local_ip", ip, 20);
    				ret=check_input_ip(ip,len);
    				
    				if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的本地ip地址!</td></tr>");	
	    				goto END;
    				}
    				
    				cgiFormStringSpaceNeeded("local_netmask", &len);
    				cgiFormStringNoNewlines("local_netmask", netmask_set, 20);
    				ret=check_input_ip(netmask_set,len);
    				
    				if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的子网掩码!</td></tr>");	
	    				goto END;
    				}
    				
    				cgiFormStringSpaceNeeded("local_gateway", &len);
    				cgiFormStringNoNewlines("local_gateway", gateway_set, 20);
    				ret=check_input_ip(gateway_set,len);
    				
    				if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的网关!</td></tr>");	
	    				goto END;
    				}
    				
    				
    				cgiFormStringSpaceNeeded("local_macadd", &len);
    				cgiFormStringNoNewlines("local_macadd", macaddr_set, 20);
    				ret=check_input_macadd(macaddr_set,len);
    				
    				if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的物理地址!</td></tr>");	
	    				goto END;
    				}
  
						if((fp_mac=fopen("/mnt/nand1-1/config/macadress","w"))==NULL)
							{
							//printf("can not open command passwd the file.\n");
							return -1;
							}
									
						fprintf(fp_mac,"#!/bin/sh\n");	
						fprintf(fp_mac,"/sbin/ifconfig eth0 %s netmask %s broadcast 192.168.1.255\n",ip,netmask_set);							
						fprintf(fp_mac,"/sbin/ifconfig eth0 hw ether %s\n",macaddr_set);
						
						fclose(fp_mac);
						
						write_file ( "/mnt/nand1-1/config/ipadress",ip);
						write_file ( "/mnt/nand1-1/config/netmask",netmask_set);
						write_file ( "/mnt/nand1-1/config/gateway",gateway_set);
						write_file ( "/mnt/nand1-1/config/macaddr",macaddr_set);
						
						ret=SetipAddr("eth0",ip,netmask_set,gateway_set);	
						
						if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的ip!</td></tr>");	
	    				goto END;
    				}			    				
						ether_atoe(macaddr_set,mac_add);
						
    				
						ret=set_mac_addr("eth0",mac_add);
						if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的物理地址!</td></tr>");	
	    				goto END;
    				}	
  					
  					
						inet_setroute("192.168.0.0","255.255.0.0");
						inet_setroute("224.0.0.0","224.0.0.0");
						
						strcpy(ip_now,ip);
						strcpy(netmask_now,netmask_set);
						strcpy(gateway_now,gateway_set);
						strcpy(macaddr_now,macaddr_set);
						
       
    } 
    
      if ((cgiFormSubmitClicked("icam_par_set") == cgiFormSuccess))  
    {
    	
    					//cgiFormSelectSingle("select_type", def_type, 2, &TypeChoice, 0);
							//fprintf(cgiOut, "I am: %s<BR>\n", def_type[TypeChoice]);
    					
    				cgiFormStringSpaceNeeded("icam_ipaddr", &len);
    				cgiFormStringNoNewlines("icam_ipaddr", icam_ip, 20);

						ret=check_input_ip(icam_ip,len);
						
    				if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的摄像头ip地址!</td></tr>");	
	    				goto END;
    				}
    				
    				
    				cgiFormStringSpaceNeeded("iint_time", &len);
    				cgiFormStringNoNewlines("iint_time", iter_num, 20);
    				
    				if((len-1) == 2 || (len-1) == 1)
    				{
    					for(i=0;i<len-1;i++)
    					{
    						if( iter_num[i]<'0' || iter_num[i]>'9' )
    							{
    									fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
					    				请输正确的时间!</td></tr>");	
					    				goto END;
    							}
    					}
    				}
    				else
    				{
    				fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
		    				请输正确的时间!</td></tr>");	
		    				goto END;	
    				}	
    					
    			
    					write_file ( "/mnt/nand1-1/config/icam_ipadd",icam_ip);
										
			    		write_file ( "/mnt/nand1-1/config/iter_tm",iter_num);
									
							sprintf(tmp_buffer,"<command>icamera_ip_set</command>");
							send_massge(tmp_buffer);
						
						
						//send_massge("<spl_get>");
					  
						ret=rec_massge();
						if(ret==0)
							{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				成功</td></tr>");
						}
						else
						{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				失败</td></tr>");
						}
			    	 	
		    				
    				
    }
    
          if ((cgiFormSubmitClicked("ocam_par_set") == cgiFormSuccess))  
    {
    	
    					//cgiFormSelectSingle("select_type", def_type, 2, &TypeChoice, 0);
							//fprintf(cgiOut, "I am: %s<BR>\n", def_type[TypeChoice]);
    					
    				cgiFormStringSpaceNeeded("ocam_ipaddr", &len);
    				cgiFormStringNoNewlines("ocam_ipaddr", ocam_ip, 20);

						ret=check_input_ip(ocam_ip,len);
						
    				if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的摄像头ip1地址!</td></tr>");	
	    				goto END;
    				}
    				
    				
    				cgiFormStringSpaceNeeded("ocam_ipaddr2", &len);
    				cgiFormStringNoNewlines("ocam_ipaddr2", ocam_ip2, 20);

						ret=check_input_ip(ocam_ip2,len);
						
    				if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的摄像头ip2地址!</td></tr>");	
	    				goto END;
    				}
    				
    				
    				
    				
    				cgiFormStringSpaceNeeded("oint_time", &len);
    				cgiFormStringNoNewlines("oint_time", oter_num, 20);
    				
    				if((len-1) == 2 || (len-1) == 1)
    				{
    					for(i=0;i<len-1;i++)
    					{
    						if( oter_num[i]<'0' || oter_num[i]>'9' )
    							{
    									fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
					    				请输正确的时间!</td></tr>");	
					    				goto END;
    							}
    					}
    				}
    				else
    				{
    				fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
		    				请输正确的时间!</td></tr>");	
		    				goto END;	
    				}	
    					
    			
    					write_file ( "/mnt/nand1-1/config/ocam_ipadd",ocam_ip);
							write_file ( "/mnt/nand1-1/config/ocam_ipadd2",ocam_ip2);
			    		write_file ( "/mnt/nand1-1/config/oter_tm",oter_num);
									
							sprintf(tmp_buffer,"<command>ocamera_ip_set</command>");
							send_massge(tmp_buffer);
						
						
						//send_massge("<spl_get>");
					  
						ret=rec_massge();
						if(ret==0)
							{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				成功</td></tr>");
						}
						else
						{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				失败</td></tr>");
						}
			    	 	
		    				
    				
    }
    
    
    
  	if(cgiFormSubmitClicked("fq_set") == cgiFormSuccess) 
    {

			Set_fanum(1);
    }
    
    if(cgiFormSubmitClicked("fq_del") == cgiFormSuccess) 
    {

			Set_fanum(0);
    }
    
    if(cgiFormSubmitClicked("fq_clean") == cgiFormSuccess) 
    {

			Alarn_Clean();
    }
    
    if(cgiFormSubmitClicked("mkjip_set") == cgiFormSuccess) 
    {
			Set_mkjip();
			
    }

    
    if(cgiFormSubmitClicked("mkjip_clen") == cgiFormSuccess) 
    {
			write_file("/mnt/nand1-1/config/mkj_state","0");
			
    }
        if(cgiFormSubmitClicked("dev_id_set") == cgiFormSuccess) 
    {
    		Set_devid();
 		}
 	 
    
    if(cgiFormSubmitClicked("sjk_set") == cgiFormSuccess) 
    {
    	
    				cgiFormStringSpaceNeeded("sjk_ip", &len);
    				cgiFormStringNoNewlines("sjk_ip", sjk_ipadd, 32);
    				ret=check_input_ip(sjk_ipadd,len);
    				
    				if(ret<0)
    				{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				请输入正确的本地ip地址!</td></tr>");	
	    				goto END;
    				}
    				
    				sprintf(tmp_buffer,"<command>spl_get</command><data>%s</data>",sjk_ipadd);
						send_massge(tmp_buffer);
					  
						ret=rec_massge();
						if(ret==0)
							{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				成功</td></tr>");
						}
						else
						{
							fprintf(cgiOut ,"<tr><td align=\"left\" width=\"20%%\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp\
	    				失败</td></tr>");
						}
		}
		
		if(cgiFormSubmitClicked("kzq_set") == cgiFormSuccess) 
    {
			
				set_kzq();
		}
    
     if(cgiFormSubmitClicked("pt_set") == cgiFormSuccess) 
    {
				set_pt();
    }
    
     if(cgiFormSubmitClicked("contime_set") == cgiFormSuccess) 
    {
				set_con_time();
    }
    
    
    
  
  
  



    END:
    ShowIndex();  
    fprintf(cgiOut, "</BODY></HTML>\n");  
   // fprintf(cgiOut, "</HTML>\n"); 
    return 0;  
}  

